#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  transformar_instancias.py
#  
#  Copyright 2016 Santiago Iturriaga - INCO <siturria@saxo.fing.edu.uy>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

dims = [50, 75, 100]
cant = 5

#workloads_orig/workload_50_10_5_100_0.L

def main(args):
    for d in dims:
        print("Dimension: {0}".format(d))

        for i in range(cant):
            print("Instancia: {0}".format(i))
            
            with open("workload_{0}_10_5_100_{1}_L.log".format(d,i), "w") as inst_i:
                for j in range(cant):
                    if not i == j:
                        with open("workloads_orig/workload_{0}_10_5_100_{1}.L.log".format(d,j)) as inst_j:
                            for line in inst_j:
                                inst_i.write(line)
    
    return 0

if __name__ == '__main__':
    import sys
            
    sys.exit(main(sys.argv))
