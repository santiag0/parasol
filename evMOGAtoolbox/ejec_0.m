global SA_GEN_START;
global SA_PROB;
global SA_VERSION;

warning('off','all');

% Semilla para la generaci�n de n�meros
%rng(2048);
rng('shuffle');

%x = dir('scenarios/workloads_red/workload_hi*');
%x = dir('scenarios/workloads_red/workload_med*');
%x = dir('scenarios/workloads_red/workload_low*');
x = dir('../instancias/0-2/*');
no_files = size(x);

ini_all = datestr(now);

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p2 g1')
            disp(i)      
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p2.txt',3,i,'scenarios/powerProfiles/g1.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p3 g1')
            disp(i)   
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p3.txt',3,i,'scenarios/powerProfiles/g1.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p4 g1')
            disp(i)   
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p4.txt',3,i,'scenarios/powerProfiles/g1.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

% =========================================================================================

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p2 g2')
            disp(i)      
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p2.txt',3,i,'scenarios/powerProfiles/g2.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p3 g2')
            disp(i)   
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p3.txt',3,i,'scenarios/powerProfiles/g2.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p4 g2')
            disp(i)   
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p4.txt',3,i,'scenarios/powerProfiles/g2.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

% =========================================================================================

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p2 g3')
            disp(i)      
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p2.txt',3,i,'scenarios/powerProfiles/g3.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p3 g3')
            disp(i)   
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p3.txt',3,i,'scenarios/powerProfiles/g3.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

for j=1:no_files(1,1)
    if x(j).isdir == 0
        disp('====================================================')
        fn = sprintf('../instancias/0-2/%s',x(j).name);
        disp(fn)
        disp(x(j).name) 
        
        for i=1:20
            disp('Ejecucion p4 g3')
            disp(i)   
            ini = datestr(now);
            evMOGA_parasol(100,1000,fn,'scenarios/powerProfiles/p4.txt',3,i,'scenarios/powerProfiles/g3.mat');
            display(['> Inicio evMOGA: ', ini]);
            display(['> Fin evMOGA   : ', datestr(now)]);
        end
    end
end

display(['> Inicio all: ', ini_all]);
display(['> Fin all   : ', datestr(now)]);