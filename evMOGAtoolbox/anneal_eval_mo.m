function [fitness,current] = anneal_eval_mo(current)
    ref_power_total = current.ref_power_total;
    ref_budget = current.ref_budget;
    ref_violations = current.ref_violations;
      
    power_total = current.sol.obj_power_total;
    budget = current.sol.obj_budget;
    violations = current.sol.obj_violations;
    
    if current.sol.makespan > 150
        fitness = 0;
    else
        if (power_total == ref_power_total && budget == ref_budget && violations == ref_violations)
            fitness = 0;
        elseif (power_total <= ref_power_total && budget <= ref_budget && violations <= ref_violations)
            fitness = 1;
            %display('YAY!!!!');
        else
            fitness = 0;
        end
    end
end

