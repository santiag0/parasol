run greenPowerProfiles

figure % new figure
ax1 = subplot(2,1,1); % top subplot
ax2 = subplot(2,1,2); % bottom subplot

x = [1:150];
y1 = GREEN_GENERATION_G1;
y2 = GREEN_GENERATION_G2;

plot(ax1,x,y1,'g')
title(ax1,'Early morning energy generation')
ylabel(ax1,'Energy (kW)')
xlabel(ax1,'Timestep')

plot(ax2,x,y2,'g')
title(ax2,'Midday energy generation')
ylabel(ax2,'Energy (kW)')
xlabel(ax2,'Timestep')
