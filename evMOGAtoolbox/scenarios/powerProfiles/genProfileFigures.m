figure % new figure
ax1 = subplot(3,1,1); % top subplot
ax2 = subplot(3,1,2); % bottom subplot
ax3 = subplot(3,1,3); % bottom subplot

x = [1:150];
y1 = p4;
y2 = p3;
y3 = p2;

plot(ax1,x,y1)
title(ax1,'Profile A')
ylabel(ax1,'Energy (kW)')
xlabel(ax1,'Timestep')

plot(ax2,x,y2)
title(ax2,'Profile B')
ylabel(ax2,'Energy (kW)')
xlabel(ax2,'Timestep')

plot(ax3,x,y3)
title(ax3,'Profile C')
ylabel(ax3,'Energy (kW)')
xlabel(ax3,'Timestep')