function printPareto(f1,f2,f3,gen,scheduler_id,workload_file,power_ref_file,file_sol,file_log,file_fig)

ip = true(size(f1));

for k=1:numel(f1)
    for h=1:numel(f1)
        if (f1(h)<f1(k) & f2(h)<f2(k) & f3(h)<f3(k))
            ip(k) = false;
        end
    end
%DEBUG
%ip
%figure
end

% Saving individual Pareto fronts is now OFF, 
% now using plotPareto for print global PF for each instance

% h = figure;
% set(h, 'Visible', 'off');
% if (gen == 0) % Final Pareto
%    if scheduler_id == 1
%        plot3(f1(ip),f2(ip),f3(ip),'g+')
%    elseif scheduler_id == 2
%        plot3(f1(ip),f2(ip),f3(ip),'r*')
%    else
%        plot3(f1(ip),f2(ip),f3(ip),'b*')
%    end
% elseif (gen == 1) % Intital Pareto
%     plot3(f1(ip),f2(ip),f3(ip),'y+')
% else % Intermediate Pareto
%     plot3(f1(ip),f2(ip),f3(ip),'m+')
% end

% xlabel('f(1) - energy profile deviation');
% ylabel('f(2) - temperature profile deviation');
% zlabel('f(3) - QoS (deadline violations)');
% grid on
% axis square

% set(h, 'Visible', 'on');

% Save as PNG: also OFF now
% FF = sprintf('%s.FP.png',file_fig);
% saveas(h,FF,'png');

% FF = sprintf('%s.FP.fig',file_fig);
% saveas(h,FF,'fig');
% set(h, 'Visible', 'off');

% Point references in file: now OFF
% f = gcf();
% dcm_obj = datacursormode(f);
% set(dcm_obj,'UpdateFcn',@showDatatips)

% Best solutions for each objective
bf1 = Inf;
bsolf1 = -1;
maxf1 = 0;
for k=1:numel(f1)
    if f1(k) < bf1
        bf1 = f1(k);
        bsolf1 = k;
    end
    if f1(k) > maxf1
        maxf1 = f1(k);
    end
end
% disp(sprintf('best f1 (sol %d): %.2f',bsolf1,bf1));
% fprintf(fid,'best f1 (sol %d): %.2f\n',bsolf1,bf1);
% FORMATO: bf1 bf1 bf1 bf2 bf2 bf2 bf3 bf3 bf3 bto1 bto2 bto3
% fprintf(fid,'%.2f %.2f %.2f ',f1(bsolf1),f2(bsolf1),f3(bsolf1));

bf2 = Inf;
bsolf2 = -1;
maxf2 = 0;
for k=1:numel(f2)
    if f2(k) < bf2
        bf2 = f2(k);
        bsolf2 = k;
    end
    if f2(k) > maxf2
        maxf2 = f2(k);
    end
end
% disp(sprintf('best f2 (sol %d): %.2f',bsolf2,bf2));
% fprintf(fid,'best f2 (sol %d): %.2f\n',bsolf2,bf2);
% fprintf(fid,'%.2f %.2f %.2f ',f1(bsolf2),f2(bsolf2),f3(bsolf2));

bf3 = Inf;
bsolf3 = -1;
maxf3 = 0;
for k=1:numel(f3)
    if f3(k) < bf3
        bf3 = f3(k);
        bsolf3 = k;
    end
    if f3(k) > maxf3
        maxf3 = f3(k);
    end
end
% disp(sprintf('best f3 (sol %d): %.2f',bsolf3,bf3));
% fprintf(fid,'best f3 (sol %d): %.2f\n',bsolf3,bf3);
% fprintf(fid,'%.2f %.2f %.2f ',f1(bsolf3),f2(bsolf3),f3(bsolf3));

% Nearest solution to ideal vector

bdist = Inf;
nsi = -1;
for k=1:numel(f1)
    if sqrt((f1(k)^2/maxf1^2)+(f2(k)^2/maxf2^2)+(f3(k)^2/maxf3^2)) < bdist
        bdist = sqrt((f1(k)^2/maxf1^2)+(f2(k)^2/maxf2^2)+(f3(k)^2/maxf3^2));
        nsi = k;
    end
end

if nsi == -1
    nsi = 1;
end

% disp(sprintf('nearest ideal: (%d - (%.2f,%.2f,%.2f)',nsi,f1(nsi),f2(nsi),f3(nsi)));
% fprintf(fid,'nearest ideal: (%d - (%.2f,%.2f,%.2f)\n',nsi,f1(nsi),f2(nsi),f3(nsi));
% fprintf(fid,'%.2f %.2f %.2f\n',f1(nsi),f2(nsi),f3(nsi));

% Save Pareto front (for computing metrics, analysis, etc.)
nf = strsplit(file_log,'.log');
to_file_name = sprintf('%s.TO',nf{1});
tradeoff_sol_obj = [f1(nsi) f2(nsi) f3(nsi)];
save(to_file_name,'tradeoff_sol_obj','-ascii','-append')

% Save Pareto front (for computing metrics, analysis, etc.)
nf = strsplit(file_log,'.log');
fp_file_name = sprintf('%s.FP',nf{1});
tfp_file_name = sprintf('%s.tFP',nf{1});

for h=1:numel(ip)
    pareto_sol_obj = [f1(h) f2(h) f3(h)];
    save(fp_file_name,'pareto_sol_obj','-ascii','-append')
    
    if ip(h) ~= false
        save(tfp_file_name,'pareto_sol_obj','-ascii','-append')
    end
end

% fid = fopen(file_log,'at+');
% % FORMATO: bf1 bf1 bf1 bf1 bf2 bf2 bf2 bf2 bf3 bf3 bf3 bf3 bto1 bto2 bto3
% % bto4
% 
% text_title = 'Best energy (f1)';
% plotSolution(bsolf1,file_sol,workload_file,power_ref_file,text_title,file_fig,fid);
% 
% text_title = 'Best temperature (f2)';
% plotSolution(bsolf2,file_sol,workload_file,power_ref_file,text_title,file_fig,fid);
% 
% text_title = 'Best QoS (f3)';
% plotSolution(bsolf3,file_sol,workload_file,power_ref_file,text_title,file_fig,fid);
% 
% text_title = 'Best trade-off';
% plotSolution(nsi,file_sol,workload_file,power_ref_file,text_title,file_fig,fid);
% 
% fprintf(fid,'\n');
% 
% fclose(fid);


