global SCHEDULER_ID;
SCHEDULER_ID = 4;

global SA_GEN_START;
global SA_PROB;
global SA_VERSION;
global SA_ITERS;

SA_GEN_START = -1;
SA_PROB = 1;
SA_VERSION = 1;
SA_ITERS = 5;

rng(2048);

improvs = zeros(20,3);

for i = 1:20
    display(i);
    display(datestr(now));
    
    f = evaluate_objective(chromosome(i,:), M, V);
    new_min_power = f(1);
    new_min_budget = f(2);
    new_min_viol = f(3);

   %improv_power =  chromosome(i, 301) - new_min_power;
   %improv_budget = chromosome(i, 302) - new_min_budget;
   %improv_violations = chromosome(i, 303) - new_min_viol;
    
    if chromosome(i, 301) == 0
        improv_power = 0;
    else
        improv_power = 1 - new_min_power / chromosome(i, 301);
    end

    improv_budget = 1 - new_min_budget / chromosome(i, 302);

    if chromosome(i, 303) == 0
        improv_violations = 0;
    else
        improv_violations = 1 - new_min_viol / chromosome(i, 303);
    end

    improvs(i,1) = improv_power;
    improvs(i,2) = improv_budget;
    improvs(i,3) = improv_violations;
    
    display(datestr(now));
end

display(improvs);

avg_power = mean(improvs(:,1));
avg_budget = mean(improvs(:,2));
avg_violations = mean(improvs(:,3));

display(avg_power);
display(avg_budget);
display(avg_violations);

% [min_power,min_power_index] = min(chromosome(:,301))
% 
% %display('Improving power...');
% %display(chromosome(min_power_index, 301));
% %display(chromosome(min_power_index, 302));
% %display(chromosome(min_power_index, 303));
% 
% f = evaluate_objective(chromosome(min_power_index,:), M, V);
% new_min_power = f(1);
% new_min_budget = f(2);
% new_min_viol = f(3);
% 
% %display('New values:');
% %display(new_min_power);
% %display(new_min_budget);
% %display(new_min_viol);
% 
% if chromosome(min_power_index, 301) == 0
%     improv_power = 0;
% else
%     improv_power = 1 - new_min_power / chromosome(min_power_index, 301);
% end
% 
% improv_budget = 1 - new_min_budget / chromosome(min_power_index, 302);
% 
% if chromosome(min_power_index, 303) == 0
%     improv_violations = 0;
% else
%     improv_violations = 1 - new_min_viol / chromosome(min_power_index, 303);
% end
% 
% display(improv_power * 100);
% display(improv_budget * 100);
% display(improv_violations * 100);

%[min_budget,min_budget_index] = min(chromosome(:,301))
%[new_min_power, new_min_budget, new_min_viol] = evaluate_objective(chromosome(min_power_index,:), M, V);

%[min_viol,min_viol_index] = min(chromosome(:,302))
%[new_min_power, new_min_budget, new_min_viol] = evaluate_objective(chromosome(min_power_index,:), M, V);
