function new = anneal_gen_mo(current)
    if current.ref_power_total > current.power_total
        current.ref_power_total = current.power_total;
    end
    
    if current.ref_budget > current.budget
        current.ref_budget = current.budget;
    end
    
    if current.ref_violations > current.violations
        current.ref_violations = current.violations;
    end

    old_sol = current.sol;

    cant_movs = 1; %randi(2,1,1);
    moves = randi(length(old_sol),cant_movs*2,1);

    new_sol = old_sol;
    
    for i = 1:cant_movs
        orig = moves(i);
        dest = moves(i+cant_movs);
        
        if orig ~= dest
            if orig < dest
                if orig > 1
                    new_sol = horzcat(new_sol(1,1:orig-1), new_sol(1,orig+1:dest-1), new_sol(1,orig), new_sol(1,dest:end));
                else
                    new_sol = horzcat(new_sol(1,orig+1:dest-1), new_sol(1,orig), new_sol(1,dest:end));
                end
            else
                if orig < length(new_sol)
                    new_sol = horzcat(new_sol(1,1:dest), new_sol(1,orig), new_sol(1,dest+1:orig-1), new_sol(1,orig+1:end));
                else
                    new_sol = horzcat(new_sol(1,1:dest), new_sol(1,orig), new_sol(1,dest+1:orig-1));
                end
            end
        end
    end
    
    new = current;
    new.sol = new_sol;
    new = anneal_metrics(new);
end

