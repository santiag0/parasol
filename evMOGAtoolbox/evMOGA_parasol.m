%% evMOGA parasol

function evMOGA_parasol(pop,gen,file_workload,file_power_ref,scheduler_id,nro_ejec,green_power)

% Semilla para la generaci�n de n�meros
%rng(2048);
%rng('shuffle');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Minimal algorithm parameters set (problem characteristics)
clear eMOGA
eMOGA.objfun='parasol_function';         % m-function name for objectives computation
eMOGA.searchspaceUB=[ones(150,1)'*300, ones(150,1)'*1920];   % Search space upper bound
eMOGA.searchspaceLB=zeros(300,1)'; % Search space lower bound
eMOGA.objfun_dim=3;            % Objective space dimension
eMOGA.Generations=gen;        % Number of generations
eMOGA.Nind_GA=8;
eMOGA.Nind_P=pop;

% additional parameter of the objective function
% in this case just to waist time 
%p.n=1000000;
%p.p=50000;
%eMOGA.param=p;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

warning('off','all');

global TIME_HORIZON;
TIME_HORIZON = 150;

global dim;
dim = TIME_HORIZON * 2;

global SA_GEN_START;
global SA_PROB;
global SA_VERSION;
global SCHEDULER_ID;

%x = dir('scenarios/workloads_red/workload_med*');
%disp(x(1).name)
%fn = sprintf('scenarios/workloads_red/%s',x(1).name);
%disp(fn)

%file_workload=fn;
%file_power_ref='scenarios/powerProfiles/p2.txt';
%green_power='scenarios/powerProfiles/g1.mat';

initialise_global_constants(file_workload,file_power_ref,green_power);

SCHEDULER_ID = scheduler_id;
%SCHEDULER_ID = 3;
%SCHEDULER_ID = 1;

%% Algorithm execution
ini = datestr(now);
[pfront,pset,eMOGA]=evMOGA(eMOGA);
%display(['> Inicio evMOGA: ', ini]);
%display(['Fin evMOGA: ', datestr(now)]);

%fw = strsplit(file_workload,'workload_');
fw = strsplit(file_workload,'_');
fp = strsplit(file_power_ref,'powerProfiles/');
fgp = strsplit(green_power,'powerProfiles/');

%scheduler_id = SCHEDULER_ID;
%pop = eMOGA.Nind_P;
%gen = eMOGA.Generations;

switch scheduler_id
    case 1
        scheduler_name = 'BFH';
    case 2
        scheduler_name = 'EFT';
    case 3
        scheduler_name = 'BSD';
    case 4
        scheduler_name = 'SA';
end

%name_inst = strsplit(fw{2},'.log');
name_inst = fw{2};
num_inst = fw{6};
logging_dir = sprintf('analisis_experimental/results/%s/%s/%s/%s/%s',scheduler_name,fgp{2},fp{2},name_inst,num_inst)

mkdir(logging_dir);

% Result
% Save the result in ASCII text format.
file_sol = sprintf('%s/solution.%s.%s.%s.(%d-%d).%d.sol',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec)
save(file_sol,'pfront','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec)
file_fig = sprintf('%s/fig.%s.%s.%s.(%d-%d).%d',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec);
printPareto(pfront(:,1),pfront(:,2),pfront(:,3),0,SCHEDULER_ID,file_workload,file_power_ref,file_sol,file_log,file_fig);

SCHEDULER_ID = 4;

%global SA_GEN_START;
%global SA_PROB;
%global SA_VERSION;
global SA_ITERS;

SA_GEN_START = -1;
SA_PROB = 1;
SA_VERSION = 1;
SA_ITERS = 5;

o_results = zeros(size(pfront,1),3);
n_results = zeros(size(pfront,1),3);
improvs = zeros(size(pfront,1),3);
diffs = zeros(size(pfront,1),3);

total_nd = 0;

display(['> Inicio SA: ', datestr(now)]);

for i = 1:size(pfront,1)
    nondom = 1;

    for h = 1:size(pfront,1)
        if (pfront(h, 1) < pfront(i, 1) && ...
                pfront(h, 2) < pfront(i, 2) && ...
                pfront(h, 3) < pfront(i, 3))

            nondom = 0;
        end
    end

    if nondom == 1    
        %display(i);
        %display(datestr(now));

        total_nd = total_nd + 1;

        o_results(total_nd,1) = pfront(i, 1);
        o_results(total_nd,2) = pfront(i, 2);
        o_results(total_nd,3) = pfront(i, 3);

        f = parasol_function(pset(i,:));

        if o_results(total_nd,1) >= f(1) && o_results(total_nd,2) >= f(2) && o_results(total_nd,2) >= f(2)
            new_min_power = f(1);
            new_min_budget = f(2);
            new_min_viol = f(3);
        else
            new_min_power = o_results(total_nd,1);
            new_min_budget = o_results(total_nd,2);
            new_min_viol = o_results(total_nd,3);
        end

        n_results(total_nd,1) = new_min_power;
        n_results(total_nd,2) = new_min_budget;
        n_results(total_nd,3) = new_min_viol;

        diffs(total_nd,1) = pfront(i, 1) - new_min_power;
        diffs(total_nd,2) = pfront(i, 2) - new_min_budget;
        diffs(total_nd,3) = pfront(i, 3) - new_min_viol;

        if pfront(i, 1) == 0
            improvs(total_nd,1) = 0;
        else
            improvs(total_nd,1) = 1 - new_min_power / pfront(i, 1);
        end

        improvs(total_nd,2) = 1 - new_min_budget / pfront(i, 2);

        if pfront(total_nd, 3) == 0
            improvs(total_nd,3) = 0;
        else
            improvs(total_nd,3) = 1 - new_min_viol / pfront(i, 3);
        end  
    end
end

display(['> Fin SA: ', datestr(now)]);

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.nFP.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec);
save(file_log,'n_results','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.diff.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec);
save(file_log,'diffs','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.impr.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec);
save(file_log,'improvs','-ascii','-append');

logging_dir = sprintf('analisis_experimental/results/%s',scheduler_name);

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).means.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen);
means = [mean(improvs(1:total_nd,1)) mean(improvs(1:total_nd,2)) mean(improvs(1:total_nd,3))];
save(file_log,'means','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).stdev.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen);
stdev = [std(improvs(1:total_nd,1)) std(improvs(1:total_nd,2)) std(improvs(1:total_nd,3))];
save(file_log,'stdev','-ascii','-append');
