#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
from os import path

import numpy
from scipy import stats

DIMS=[50,75,100]

prefix = "pfronts"

POWER_PROFILE = [316500.0, 316500.0, 253200.0]
PROFILE_BAU_BUDGET = (661370.0,957300.0,1400000.0)

def main():
    r_a = []

    for d in range(3):
        r_d_budget = []
        r_d_power = []

        for g in range(1,4):
            r_budget = []
            r_power = []

            for p in range(2,5):
                with open('{0}/qosbgt-evmoga-p{1}-g{2}-{3}.log'.format(prefix,p,g,DIMS[d])) as f:
                    for line in f:
                        vals = line.strip().split()
                        if len(vals) == 3:
                            rval = float(vals[1].strip()) / PROFILE_BAU_BUDGET[g-1]
                            r_budget.append(rval)
                            r_d_budget.append(rval)

                with open('{0}/qosbgt-evmoga-p{1}-g{2}-{3}.log'.format(prefix,p,g,DIMS[d])) as f:
                    # with open('{0}/qospwr-evmoga-p{1}-g{2}-{3}.log'.format(prefix,p,g,DIMS[d])) as f:
                    for line in f:
                        vals = line.strip().split()
                        if len(vals) == 3:
                            rval = float(vals[0].strip()) / POWER_PROFILE[p-2]
                            r_power.append(rval)
                            r_d_power.append(rval)

            r_avg_budget = numpy.average(r_budget)
            r_std_budget = numpy.std(r_budget)

            r_avg_power = numpy.average(r_power)
            r_std_power = numpy.std(r_power)

            print('{2} & g{1} & {3:.2f}$\pm${4:.2f} & {5:.2f}$\pm${6:.2f} \\\\'.format(p, g, DIMS[d], 1-r_avg_budget, r_std_budget, r_avg_power, r_std_power))

        r_a.append((DIMS[d], numpy.average(r_d_budget), numpy.average(r_d_power)))

    for a in r_a:
        print('{0} {1:.2f} {2:.2f}'.format(a[0], 1-a[1], a[2]))

    return 0

if __name__ == '__main__':
    main()
