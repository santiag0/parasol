#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
from os import path

import numpy
from scipy import stats

DIMS=[50,75,100]

prefix = "pfronts"
pvalue = 0.001

def main():    
    for d in range(3):
        for g in range(1,4):
            evmoga = []
            r_evmoga = []
            nsgaii = []
            r_nsgaii = []

            #print('g{1} p{0} {2}'.format(p, g, DIMS[d]))

            for p in range(2,5):    
                for i in range(3):
                    pf = 0.0

                    with open('{0}/hv-pf-p{1}-g{2}-{3}-{4}.log'.format(prefix,p,g,DIMS[d],i)) as f:
                        line = f.readline()
                        pf = float(line.strip())

                    #print(pf)
                    
                    with open('{0}/hvaa-evmoga-p{1}-g{2}-{3}-{4}.log'.format(prefix,p,g,DIMS[d],i)) as f:
                        lines = f.readlines()
                        for line in lines:
                            fvalue = float(line.strip())
                            evmoga.append(fvalue)
                            r_evmoga.append(fvalue / pf)

                    with open('{0}/hvaa-nsgaii-p{1}-g{2}-{3}-{4}.log'.format(prefix,p,g,DIMS[d],i)) as f:
                        lines = f.readlines()
                        for line in lines:
                            fvalue = float(line.strip())
                            nsgaii.append(fvalue)
                            r_nsgaii.append(fvalue / pf)

            #print(evmoga)
            #print(r_evmoga)
            #print(numpy.average(r_evmoga))
            r_med_evmoga = numpy.median(r_evmoga)
            r_p25_evmoga = numpy.percentile(r_evmoga, 25)
            r_p75_evmoga = numpy.percentile(r_evmoga, 75)
            r_iqr_evmoga = r_p75_evmoga - r_p25_evmoga

            #print(nsgaii)
            #print(r_nsgaii)
            #print(numpy.average(r_nsgaii))
            r_med_nsgaii = numpy.median(r_nsgaii)
            r_p25_nsgaii = numpy.percentile(r_evmoga, 25)
            r_p75_nsgaii = numpy.percentile(r_evmoga, 75)
            r_iqr_nsgaii = r_p75_nsgaii - r_p25_nsgaii

            sign_evmoga = False
            sign_nsgaii = False

            (sts, pv) = stats.kruskal(r_evmoga, r_nsgaii)
            if pv <= pvalue:
                if r_avg_evmoga > r_avg_nsgaii:
                    #print("evmoga")
                    sign_evmoga = True
                else:
                    #print("nsgaii")
                    sign_nsgaii = True
            else:
                #print("N/A")
                pass

            if sign_evmoga:
                print('{2} & g{1} & \\textbf{{{3:.2f}$\pm${4:.2f}}} & {5:.2f}$\pm${6:.2f} \\\\'.format(p, g, DIMS[d], r_med_evmoga, r_iqr_evmoga, r_med_nsgaii, r_iqr_nsgaii))
            elif sign_nsgaii:
                print('{2} & g{1} & {3:.2f}$\pm${4:.2f} & \\textbf{{{5:.2f}$\pm${6:.2f}}} \\\\'.format(p, g, DIMS[d], r_med_evmoga, r_iqr_evmoga, r_med_nsgaii, r_iqr_nsgaii))
            else:
                print('{2} & g{1} & {3:.2f}$\pm${4:.2f} & {5:.2f}$\pm${6:.2f} \\\\'.format(p, g, DIMS[d], r_med_evmoga, r_iqr_evmoga, r_med_nsgaii, r_iqr_nsgaii))

            #print('{0:.2e}'.format(pv))
            #print('>>')
    
    return 0

if __name__ == '__main__':
    main()
