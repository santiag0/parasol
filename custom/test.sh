#python3 pf_gather.py 'log.50.p2.txt.BSD.(100-1000).' '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g1.mat/p2.txt/50/0 > evmoga-p2-50-0.log
#
#python3 pf_gather.py 'log.50.p2.txt.BSD.(20-1000).' '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g1.mat/p2.txt/50/0 > nsgaii-p2-50-0.log
#
#python3 pf_gather.py 'log.75.p2.txt.BSD.(100-1000).' '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g1.mat/p2.txt/75/0 > evmoga-p2-75-0.log
#
#python3 pf_gather.py 'log.75.p2.txt.BSD.(20-1000).' '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g1.mat/p2.txt/75/0 > nsgaii-p2-75-0.log
#
#python3 pf_gather.py 'log.100.p2.txt.BSD.(100-1000).' '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g1.mat/p2.txt/100/0 > evmoga-p2-100-0.log
#
#python3 pf_gather.py 'log.100.p2.txt.BSD.(20-1000).' '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g1.mat/p2.txt/100/0 > nsgaii-p2-100-0.log
#
#python3 pf_gather_2.py 3 evmoga-p2-75-0.log nsgaii-p2-75-0.log > pf-p2-75-0.log
#python3 pf_gather_2.py 3 evmoga-p2-100-0.log nsgaii-p2-100-0.log > pf-p2-100-0.log

DIM[0]=50
DIM[1]=75
DIM[2]=100

path_prefix="pfronts"

for (( i=0; i<=0; i++ ))
do
    for (( d=1; d<=1; d++ ))
    do
        for (( g=2; g<=2; g++ ))
        do
            for (( p=2; p<=2; p++ ))
            do
                #echo "python3 pf_gather.py log.${DIM[d]}.p${p}.txt.BSD.(100-1000). .nFP.log 20 3 /home/siturria/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i} > ${path_prefix}/evmoga-p${p}-g${g}-${DIM[d]}-${i}.log"
                #python3 pf_gather.py "log.${DIM[d]}.p${p}.txt.BSD.(100-1000)." '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i} > ${path_prefix}/evmoga-p${p}-g${g}-${DIM[d]}-${i}.log
                
                echo "python3 pf_gather.py log.${DIM[d]}.p${p}.txt.BSD.(20-1000). .nFP.log 20 3 /home/siturria/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i} > ${path_prefix}/nsgaii-p${p}-g${g}-${DIM[d]}-${i}.log"
                python3 pf_gather.py "log.${DIM[d]}.p${p}.txt.BSD.(20-1000)." '.nFP.log' 20 3 /home/siturria/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i} > ${path_prefix}/nsgaii-p${p}-g${g}-${DIM[d]}-${i}.log

                echo "python3 pf_gather_2.py 3 ${path_prefix}/evmoga-p${p}-g${g}-${DIM[d]}-${i}.log ${path_prefix}/nsgaii-p${p}-g${g}-${DIM[d]}-0.log > ${path_prefix}/pf-p${p}-g${g}-${DIM[d]}-${i}.log"
                python3 pf_gather_2.py 3 ${path_prefix}/evmoga-p${p}-g${g}-${DIM[d]}-${i}.log ${path_prefix}/nsgaii-p${p}-g${g}-${DIM[d]}-${i}.log > ${path_prefix}/pf-p${p}-g${g}-${DIM[d]}-${i}.log

                echo "python3 pf_hv_w.py 3 ${path_prefix}/evmoga-p${p}-g${g}-${DIM[d]}-${i}.log ${path_prefix}/nsgaii-p${p}-g${g}-${DIM[d]}-0.log > ${path_prefix}/w-p${p}-g${g}-${DIM[d]}-${i}.log"
                python3 pf_hv_w.py 3 ${path_prefix}/evmoga-p${p}-g${g}-${DIM[d]}-${i}.log ${path_prefix}/nsgaii-p${p}-g${g}-${DIM[d]}-${i}.log > ${path_prefix}/w-p${p}-g${g}-${DIM[d]}-${i}.log                

                echo "======================================================="
            done
        done
    done
done 
