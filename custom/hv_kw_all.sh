DIM[0]=50
DIM[1]=75
DIM[2]=100

path_prefix="pfronts"

rm results_hv_pv_all.log

for (( i=0; i<=2; i++ ))
do
    for (( d=0; d<=2; d++ ))
    do
        for (( g=1; g<=3; g++ ))
        do
            for (( p=2; p<=4; p++ ))
            do
                python3 hv_kw.py ${path_prefix}/hvaa-nsgaii-p${p}-g${g}-${DIM[d]}-${i}.log ${path_prefix}/hvaa-evmoga-p${p}-g${g}-${DIM[d]}-${i}.log > ${path_prefix}/hvaa-pv-p${p}-g${g}-${DIM[d]}-${i}.log

                echo "p${p} g${g} ${DIM[d]} ${i} $(cat pv-p${p}-g${g}-${DIM[d]}-${i}.log)" >> results_hv_pv_all.log
            done
        done
    done
done 
