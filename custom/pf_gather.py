#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
from os import path

sys.path.append('../libs')
import generic_pf_metrics

def load_jmetal_results(path_to_results, prefix, suffix, num_objectives, num_runs):
    results = []
    
    for run in range(num_runs):
        path_to_file = path.join(path_to_results, "{0}{1}{2}".format(prefix, run+1, suffix))

        with open(path_to_file) as f:
            lines = f.readlines()
            
            for line in lines:
                tokens = line.split()
                float_tokens = []

                for no in range(num_objectives):
                    val = float(tokens[no])
                    if val > 0.0:
                        float_tokens.append(float(tokens[no]))
                    else:
                        float_tokens.append(0.0)

                #print("{0}: {1} {2}".format(path_to_file,float_tokens,line))

                if (float_tokens > [0.0,0.0,0.0]):                        
                    results.append(float_tokens)

    return results

def main():
    if len(sys.argv) < 6:
        print("This script loads a set of solutions from different experiments and computes the aggregated Pareto front.")
        print("Not enough arguments. Usage: {0} <name prefix> <name suffix> <num runs> <num obj> <result path 1> <result path 2> <result path 3> ...".format(sys.argv[0]))
        exit(-1)   

    prefix = sys.argv[1]
    suffix = sys.argv[2]
    num_runs = int(sys.argv[3])
    num_objectives = int(sys.argv[4])
    
    points = []

    for p in range(5, len(sys.argv)):
        aux_points = load_jmetal_results(sys.argv[p], prefix, suffix, num_objectives, num_runs)
        points = points + aux_points

    #for p in points:
    #    print(p)

    generic_pf_metrics.print_non_dominated(points)
        
    return 0

if __name__ == '__main__':
    main()

