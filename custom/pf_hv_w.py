#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
from os import path

sys.path.append('../libs')
import generic_pf_metrics

def load_jmetal_results(num_objectives, path_to_file):
    results = []

    with open(path_to_file) as f:
        lines = f.readlines()
        
        for line in lines:
            tokens = line.split()
            float_tokens = []

            for no in range(num_objectives):
                val = float(tokens[no])
                if val > 0.0:
                    float_tokens.append(float(tokens[no]))
                else:
                    float_tokens.append(0.0)
                    
            results.append(float_tokens)

    return results

def main():
    if len(sys.argv) < 3:
        print("This script selects the best W point for hypervolume computation.")
        print("Not enough arguments. Usage: {0} <num objs> <data set 1> <data set 2>...".format(sys.argv[0]))
        exit(-1)   

    num_objectives = int(sys.argv[1])
    
    points = []

    for p in range(2, len(sys.argv)):
        aux_points = load_jmetal_results(num_objectives, sys.argv[p])
        points = points + aux_points

    w = points[0]
    for p in points:
        for d in range(len(p)):
            if p[d] > w[d]:
                w[d] = p[d]

    for d in range(len(p)):
        #if w[d] == 0.0:
        w[d] = w[d] + 1E-6
        #else:
        #    w[d] = w[d] + (w[d]/100.0)
        print("{0} ".format(w[d]), end="")

    #print(w)
        
    return 0

if __name__ == '__main__':
    main()

