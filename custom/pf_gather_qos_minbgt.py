#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
from os import path

def load_results(num_objectives, max_qos, path_to_file):
    budget_sol = None

    with open(path_to_file) as f:
        lines = f.readlines()

        for line in lines:
            tokens = line.split()
            float_tokens = []

            for no in range(num_objectives):
                val = float(tokens[no])
                if val > 0.0:
                    float_tokens.append(float(tokens[no]))
                else:
                    float_tokens.append(0.0)

            if (float_tokens > [0.0,0.0,0.0]):
                if (float_tokens[2] <= max_qos):
                    if budget_sol == None:
                        budget_sol = float_tokens
                    elif budget_sol[1] > float_tokens[1]:
                        budget_sol = float_tokens

    return budget_sol

def main():
    if len(sys.argv) < 4:
        print("This script filters and discards solution with QoS worse than <max qos>.")
        print("Not enough arguments. Usage: {0} <num obj> <max qos> <file>".format(sys.argv[0]))
        exit(-1)

    num_objectives = int(sys.argv[1])
    max_qos = float(sys.argv[2])
    file_path = sys.argv[3].strip()

    p = load_results(num_objectives, max_qos, file_path)

    if p != None:
        print("{0} {1} {2}".format(p[0],p[1],p[2]))

    return 0

if __name__ == '__main__':
    main()
