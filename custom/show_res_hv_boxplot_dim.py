#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
from os import path

import numpy
#from scipy import stats

import matplotlib
import matplotlib.pyplot as plt

DIMS=[50,75,100]

prefix = "pfronts"

def main():
    font = {'size'   : 22}
    matplotlib.rc('font', **font)

    # Get current size
    fig_size = plt.rcParams["figure.figsize"]

    # Prints: [8.0, 6.0]
    #print "Current size:", fig_size

    # Set figure width to 12 and height to 9
    fig_size[0] = 7
    fig_size[1] = 7.5
    plt.rcParams["figure.figsize"] = fig_size

    for d in range(3):
        evmoga = []
        r_evmoga = []
        nsgaii = []
        r_nsgaii = []

        for g in range(1,4):
            for p in range(2,5):
                for i in range(3):
                    pf = 0.0

                    with open('{0}/hv-pf-p{1}-g{2}-{3}-{4}.log'.format(prefix,p,g,DIMS[d],i)) as f:
                        line = f.readline()
                        pf = float(line.strip())

                    with open('{0}/hvaa-evmoga-p{1}-g{2}-{3}-{4}.log'.format(prefix,p,g,DIMS[d],i)) as f:
                        lines = f.readlines()
                        for line in lines:
                            fvalue = float(line.strip())
                            evmoga.append(fvalue)
                            r_evmoga.append(fvalue / pf)

                    with open('{0}/hvaa-nsgaii-p{1}-g{2}-{3}-{4}.log'.format(prefix,p,g,DIMS[d],i)) as f:
                        lines = f.readlines()
                        for line in lines:
                            fvalue = float(line.strip())
                            nsgaii.append(fvalue)
                            r_nsgaii.append(fvalue / pf)

        #print(evmoga)
        #print(r_evmoga)

        #print(nsgaii)
        #print(r_nsgaii)
        #plt.boxplot([r_evmoga, r_nsgaii], notch=True, sym='', labels=['evMOGA','NSGA-II'])
        plt.boxplot([r_evmoga, r_nsgaii], notch=True, sym='')
        plt.xlabel('Evolutionary algorithms')
        plt.xticks([1,2],('ev-MOGA', 'NSGA-II'))
        plt.ylabel('Relative Hypervolume')
        #plt.set_aspect(1.5)
        #plt.boxplot(r_evmoga)
        plt.savefig('evmoga-vs-nsgaii-d{0}.pdf'.format(DIMS[d]))
        plt.figure()

    return 0

if __name__ == '__main__':
    main()
