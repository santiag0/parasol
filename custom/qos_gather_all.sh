DIM[0]=50
DIM[1]=75
DIM[2]=100

path_prefix="pfronts"

for (( d=0; d<=2; d++ ))
do
	for (( g=1; g<=3; g++ ))
	do
		#for (( p=2; p<=4; p++ ))
		#do
			rm ${path_prefix}/qosbgt-avg-evmoga-g${g}-${DIM[d]}.log
			rm ${path_prefix}/qosbgt-avg-nsgaii-g${g}-${DIM[d]}.log
		
			python3 pf_gather_qos_minbgt_2.py ${g} ${path_prefix}/qosbgt-evmoga-g${g}-${DIM[d]}.log >> ${path_prefix}/qosbgt-avg-evmoga-g${g}-${DIM[d]}.log
			python3 pf_gather_qos_minbgt_2.py ${g} ${path_prefix}/qosbgt-nsgaii-g${g}-${DIM[d]}.log >> ${path_prefix}/qosbgt-avg-nsgaii-g${g}-${DIM[d]}.log
            
            rm ${path_prefix}/qospwr-avg-evmoga-g${g}-${DIM[d]}.log
			rm ${path_prefix}/qospwr-avg-nsgaii-g${g}-${DIM[d]}.log

			python3 pf_gather_qos_minpwr_2.py ${p} ${path_prefix}/qospwr-evmoga-g${g}-${DIM[d]}.log >> ${path_prefix}/qospwr-avg-evmoga-g${g}-${DIM[d]}.log
			python3 pf_gather_qos_minpwr_2.py ${p} ${path_prefix}/qospwr-nsgaii-g${g}-${DIM[d]}.log >> ${path_prefix}/qospwr-avg-nsgaii-g${g}-${DIM[d]}.log
            
            echo "g${g} ${DIM[d]}"
        #done
    done
done 
