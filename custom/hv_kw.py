#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
from os import path
from scipy import stats

def load_results(path_to_file):
    results = []
    
    with open(path_to_file) as f:
        lines = f.readlines()
        
        for line in lines:
            results.append(float(line.strip()))

    return results

def compute_kw(file1, file2):
    points = []
    points.append(load_results(file1))
    points.append(load_results(file2))

    pvalue = stats.kruskal(points[0], points[1])[1]

    return pvalue

def main():
    if len(sys.argv) != 3:
        print("This script loads a set of solutions from different experiments and computes the aggregated Pareto front.")
        print("Not enough arguments. Usage: {0} <file 1> <file 2>".format(sys.argv[0]))
        exit(-1)   

    pvalue = compute_kw(sys.argv[1],sys.argv[2])
    print(pvalue)
        
    return 0

if __name__ == '__main__':
    main()
