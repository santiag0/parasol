DIM[0]=50
DIM[1]=75
DIM[2]=100

QOS[0]=103
QOS[1]=152
QOS[2]=198

path_prefix="pfronts"

for (( d=0; d<=2; d++ ))
do
	for (( g=1; g<=3; g++ ))
	do
		for (( p=2; p<=4; p++ ))
		do
			rm pfronts/qosbgt-nsgaii-p${p}-g${g}-${DIM[d]}.log
			rm pfronts/qosbgt-evmoga-p${p}-g${g}-${DIM[d]}.log

			rm pfronts/qospwr-nsgaii-p${p}-g${g}-${DIM[d]}.log
			rm pfronts/qospwr-evmoga-p${p}-g${g}-${DIM[d]}.log
		
            for (( i=0; i<=2; i++ ))
			do
                #for (( n=1; n<=20; n++ ))
                #do
					## NSGA-II
                    #echo "python3 pf_gather_qos.py 3 ${QOS[d]} ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.nFP.log > ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.QoSnFP.log"
                    #python3 pf_gather_qos.py 3 ${QOS[d]} ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.nFP.log > ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.QoSnFP.log
                    
                    #echo "python3 pf_gather_qos_minbgt.py 3 ${QOS[d]} ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.nFP.log > ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.qosbgt.log"
                    #python3 pf_gather_qos_minbgt.py 3 ${QOS[d]} ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.nFP.log > ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.qosbgt.log
                    
                    #echo "python3 pf_gather_qos_minpwr.py 3 ${QOS[d]} ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.nFP.log > ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.qospwr.log"
                    #python3 pf_gather_qos_minpwr.py 3 ${QOS[d]} ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.nFP.log > ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).${n}.qospwr.log
                    
                    ## ev-MOGA
                    #echo "python3 pf_gather_qos.py 3 ${QOS[d]} ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.nFP.log > ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.QoSnFP.log"
                    #python3 pf_gather_qos.py 3 ${QOS[d]} ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.nFP.log > ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.QoSnFP.log
                    
                    #echo "python3 pf_gather_qos_minbgt.py 3 ${QOS[d]} ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.nFP.log > ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.qosbgt.log"
                    #python3 pf_gather_qos_minbgt.py 3 ${QOS[d]} ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.nFP.log > ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.qosbgt.log
                    
                    #echo "python3 pf_gather_qos_minpwr.py 3 ${QOS[d]} ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.nFP.log > ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.qospwr.log"
                    #python3 pf_gather_qos_minpwr.py 3 ${QOS[d]} ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.nFP.log > ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).${n}.qospwr.log

                    #echo "------------------------"
                #done
                
				cat ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).*.qosbgt.log >> pfronts/qosbgt-nsgaii-p${p}-g${g}-${DIM[d]}.log
				cat ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).*.qosbgt.log >> pfronts/qosbgt-evmoga-p${p}-g${g}-${DIM[d]}.log

				cat ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).*.qospwr.log >> pfronts/qospwr-nsgaii-p${p}-g${g}-${DIM[d]}.log
				cat ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).*.qospwr.log >> pfronts/qospwr-evmoga-p${p}-g${g}-${DIM[d]}.log

				#cat ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).*.qosbgt.log >> pfronts/qosbgt-nsgaii-g${g}-${DIM[d]}.log
				#cat ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).*.qosbgt.log >> pfronts/qosbgt-evmoga-g${g}-${DIM[d]}.log

				#cat ~/bitbucket/parasol/NSGAII/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(20-1000\).*.qospwr.log >> pfronts/qospwr-nsgaii-g${g}-${DIM[d]}.log
				#cat ~/bitbucket/parasol/evMOGAtoolbox/analisis_experimental/results/BSD/g${g}.mat/p${p}.txt/${DIM[d]}/${i}/log.${DIM[d]}.p${p}.txt.BSD.\(100-1000\).*.qospwr.log >> pfronts/qospwr-evmoga-g${g}-${DIM[d]}.log    
            done
        done
    done
done 
