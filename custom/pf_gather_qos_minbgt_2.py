#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pf_gather.py
#
#  This script loads a set of jMetal solutions from different
#  experiments and computes the aggregated Pareto front.
#
#  Copyright 2015 Santiago Iturriaga
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
from os import path
import numpy

PROFILE_BAU_BUDGET = (661370.0,957300.0,1400000.0)

def load_results(profile_num, path_to_file):
    sols = []
    best = None

    with open(path_to_file) as f:
        lines = f.readlines()

        for line in lines:
            tokens = line.split()

            if len(tokens) == 3:
                rbudget = float(tokens[1]) / PROFILE_BAU_BUDGET[profile_num-1]
                sols.append(rbudget)

                if best == None:
                    best = rbudget
                elif best > rbudget:
                    best = rbudget

    return (best,numpy.average(sols),numpy.std(sols))

def main():
    if len(sys.argv) < 3:
        print("This script get the best average and standard deviation budget from a QoS filtered solutions.")
        print("Not enough arguments. Usage: {0} <profile num> <file>".format(sys.argv[0]))
        exit(-1)

    profile_num = int(sys.argv[1])
    file_path = sys.argv[2].strip()

    if profile_num < 1 or profile_num > 3:
        print("Error profile_num {0} does not exist".format(profile_num))
        exit(-1)

    (best, avg, std) = load_results(profile_num, file_path)
    print("{0} {1} {2}".format(best, avg, std))

    return 0

if __name__ == '__main__':
    main()
