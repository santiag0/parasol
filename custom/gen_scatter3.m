hFig = figure();

scatter3(evmoga_g1(:,1),evmoga_g1(:,2),evmoga_g1(:,3))
hold on
scatter3(nsgaii_g1(:,1),nsgaii_g1(:,2),nsgaii_g1(:,3));

set(hFig, 'Position', [0 0 384 384])
hleg1=legend('ev-MOGA','NSGA-II');
set(hleg1,'Location','NorthEast');
title('')
xlabel('Prof. deviation');
ylabel('Budget');
zlabel('Total violation time');
set(findobj('type','axes'),'fontsize',16) 
xlhand = get(gca,'xlabel')
set(xlhand,'fontsize',16) 
ylhand = get(gca,'ylabel')
set(ylhand,'fontsize',16) 
zlhand = get(gca,'zlabel')
set(zlhand,'fontsize',16) 
view(37,20)

 %-------------------------------------------------------
hFig = figure();

scatter3(evmoga_g2(:,1),evmoga_g2(:,2),evmoga_g2(:,3))
hold on
scatter3(nsgaii_g2(:,1),nsgaii_g2(:,2),nsgaii_g2(:,3));

set(hFig, 'Position', [0 0 384 384])
hleg1=legend('ev-MOGA','NSGA-II');
set(hleg1,'Location','NorthEast');
title('')
xlabel('Profile deviation');
ylabel('Budget');
zlabel('Total violation time');
set(findobj('type','axes'),'fontsize',16) 
xlhand = get(gca,'xlabel')
set(xlhand,'fontsize',16) 
ylhand = get(gca,'ylabel')
set(ylhand,'fontsize',16) 
zlhand = get(gca,'zlabel')
set(zlhand,'fontsize',16) 
view(37,20)

 %------------------------------------------------------- 
hFig = figure();

scatter3(evmoga_g3(:,1),evmoga_g3(:,2),evmoga_g3(:,3))
hold on
scatter3(nsgaii_g3(:,1),nsgaii_g3(:,2),nsgaii_g3(:,3));

set(hFig, 'Position', [0 0 384 384])
hleg1=legend('ev-MOGA','NSGA-II');
set(hleg1,'Location','NorthEast');
title('')
xlabel('Profile deviation');
ylabel('Budget');
zlabel('Total violation time');
set(findobj('type','axes'),'fontsize',16) 
xlhand = get(gca,'xlabel')
set(xlhand,'fontsize',16) 
ylhand = get(gca,'ylabel')
set(ylhand,'fontsize',16) 
zlhand = get(gca,'zlabel')
set(zlhand,'fontsize',16) 
view(37,20)
