warning('off','all');

x = dir('scenarios/workloads_red/workload_*');
no_files = size(x);
for j=1:no_files(1,1)
     disp(x(j).name)
     
     fn = sprintf('scenarios/workloads_red/%s',x(j).name);
     disp(fn)
     
     for i=1:3
         display(['> Inicio BFH: ', datestr(now)]);
         nsga_2(30,250,fn,'scenarios/powerProfiles/p4.txt',1,i); % BFH
         display(['Fin BFH: ', datestr(now)]);
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(30,250,fn,'scenarios/powerProfiles/p4.txt',2,i); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         display(['> Inicio BFH-D: ', datestr(now)]);
         nsga_2(30,250,fn,'scenarios/powerProfiles/p4.txt',3,i); % BFH-D
         display(['Fin BFH-D: ', datestr(now)]);
         %display(['> Inicio SA: ', datestr(now)]);
         %nsga_2(30,250,fn,'scenarios/powerProfiles/p4.txt',4,i); % SA
         %display(['Fin SA: ', datestr(now)]);
     end
    
    for i=1:3
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(30,250,fn,'scenarios/powerProfiles/p3.txt',1,i); % BFH
        display(['Fin BFH: ', datestr(now)]);
        display(['> Inicio EFTH: ', datestr(now)]);
        nsga_2(30,250,fn,'scenarios/powerProfiles/p3.txt',2,i); % EFTH
        display(['Fin EFTH: ', datestr(now)]);
        display(['> Inicio BFH-D: ', datestr(now)]);
        nsga_2(30,250,fn,'scenarios/powerProfiles/p3.txt',3,i); % BFH-D
        display(['Fin BFH-D: ', datestr(now)]);
        %display(['> Inicio SA: ', datestr(now)]);
        %nsga_2(30,250,fn,'scenarios/powerProfiles/p3.txt',4,i); % SA
        %display(['Fin SA: ', datestr(now)]);
    end
    
    for i=1:3
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(30,250,fn,'scenarios/powerProfiles/p2.txt',1,i); % BFH
        display(['Fin BFH: ', datestr(now)]);
        display(['> Inicio EFTH: ', datestr(now)]);
        nsga_2(30,250,fn,'scenarios/powerProfiles/p2.txt',2,i); % EFTH
        display(['Fin EFTH: ', datestr(now)]);
        display(['> Inicio BFH-D: ', datestr(now)]);
        nsga_2(30,250,fn,'scenarios/powerProfiles/p2.txt',3,i); % BFH-D
        display(['Fin BFH-D: ', datestr(now)]);
        %display(['> Inicio SA: ', datestr(now)]);
        %nsga_2(30,250,fn,'scenarios/powerProfiles/p2.txt',4,i); % SA
        %display(['Fin SA: ', datestr(now)]);
    end
end