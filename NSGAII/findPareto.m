function findPareto(file_sols)

% Fing Pareto front from file of nondominated solutions (files *FP,
% obtained in n independent executions)

sols = load(file_sols,'-ascii');
ip = true(numel(sols(:,1)),1);

for k=1:numel(sols(:,1))
    for h=1:numel(sols(:,1))
        if (sols(h,1)<sols(k,1) & sols(h,2)<sols(k,2) & sols(h,3)<sols(k,3))
            ip(k) = false;
        end
    end
end

sP = sols(ip,:);

name_FP_global = sprintf('%s.global',file_sols)
save(name_FP_global,'sP','-ascii');

