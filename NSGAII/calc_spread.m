x = dir('scenarios/workloads/w*');
pow = ['p4';'p3';'p2'];
sch = ['BFH';'EFT';'BSD'];

no_files = size(x);
for j=1:no_files(1,1)
    % disp(x(j).name)
    fw = strsplit(x(j).name,'workload_');
    name_inst = strsplit(fw{2},'.log');
    for s=1:3
        scheduler_name = sch(s,:);
        for p=1:3
            pow_prof = pow(p,:);
            file_sols = sprintf('analisis_experimental/results/%s/%s/%s/log.%s.%s.%s.(75-500).FP.global',scheduler_name,pow_prof,name_inst{1},name_inst{1},pow_prof,scheduler_name);
            sols = load(file_sols,'-ascii');
            
            file_fp_global = sprintf('analisis_experimental/results/global_pareto_fronts/%s/%s.global.FP',pow_prof,name_inst{1});
            solsfp = load(file_fp_global,'-ascii');
            
            % Find Pareto front extremes
            [sf1,ind1] = sort(solsfp(:,1));
            ef1 = solsfp(ind1(1),:);
            [sf2,ind2] = sort(solsfp(:,2));
            ef2 = solsfp(ind2(1),:);
            [sf3,ind3] = sort(solsfp(:,3));
            ef3 = solsfp(ind3(1),:);
                        
            s = spread(sols,ef1,ef2,ef3);
            
            n_fs = strsplit(file_sols,'results/');
            name_fs = strsplit(n_fs{2},'.(75');
            out = sprintf('%s %.2f',name_fs{1},s);
            disp(out)
        end
    end
end 