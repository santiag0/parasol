function f  = genetic_operator_par(parent_chromosome, M, V, mu, mum, l_limit, u_limit)

global SIMULATION_LENGTH; global MAX_HVAC; global MAX_SERVERS;

%% function f  = genetic_operator(parent_chromosome, M, V, mu, mum, l_limit, u_limit)
% 
% This function is utilized to produce offsprings from parent chromosomes.
% The genetic operators corssover and mutation which are carried out with
% slight modifications from the original design. For more information read
% the document enclosed. 
%
% parent_chromosome - the set of selected chromosomes.
% M - number of objective functions
% V - number of decision varaiables
% mu - distribution index for crossover (read the enlcosed pdf file)
% mum - distribution index for mutation (read the enclosed pdf file)
% l_limit - a vector of lower limit for the corresponding decsion variables
% u_limit - a vector of upper limit for the corresponding decsion variables
%
% The genetic operation is performed only on the decision variables, that
% is the first V elements in the chromosome vector. 

%  Copyright (c) 2009, Aravind Seshadri
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without 
%  modification, are permitted provided that the following conditions are 
%  met:
%
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%      
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

[N,m] = size(parent_chromosome);

clear m

is_mutated = zeros(N,1);
is_crossed = zeros(N,1);

crossed_1 = zeros(N, M + V);
crossed_2 = zeros(N, M + V);
mutated = zeros(N ,M + V);

%parfor i = 1 : N
for i = 1 : N
    % With 90 % probability perform crossover
    if rand(1) < 0.9
        % Initialize the children to be null vector.
        child_1 = [];
        child_2 = [];
        % Select the first parent
        parent_1 = round(N*rand(1));
        if parent_1 < 1
            parent_1 = 1;
        end
        % Select the second parent
        parent_2 = round(N*rand(1));
        if parent_2 < 1
            parent_2 = 1;
        end
%         % Make sure both the parents are not the same. 
%         while isequal(parent_chromosome(parent_1,:),parent_chromosome(parent_2,:))
%             parent_2 = round(N*rand(1));
%             if parent_2 < 1
%                 parent_2 = 1;
%             end
%         end
        % Get the chromosome information for each randomnly selected
        % parents
        parent_1 = parent_chromosome(parent_1,:);
        parent_2 = parent_chromosome(parent_2,:);
        % Perform corssover for each decision variable in the chromosome.
        
       if rand() < 0.5
            %Let's try Single point crossover (Cristian)
            xpoint = round(rand(1,1)*V);
            child_1 = parent_1;
            child_2 = parent_2;
            child_1(1:xpoint) = parent_2(1:xpoint);
            child_2(1:xpoint) = parent_1(1:xpoint);
        else
            xpoint1 = ceil(rand(1,1)*V/2);
            xpoint2 = ceil(V/2+xpoint1);
            child_1 = parent_1;
            child_2 = parent_2;
            child_1(xpoint1:round(V/2)) = parent_2(xpoint1:round(V/2));
            child_1(xpoint2:end) = parent_2(xpoint2:end);
            child_2(xpoint1:round(V/2)) = parent_1(xpoint1:round(V/2));
            child_2(xpoint2:end) = parent_1(xpoint2:end);
       end
       
       % Evaluate the objective function for the offsprings and as before
        % concatenate the offspring chromosome with objective value.
        child_1(:,V + 1: M + V) = evaluate_objective(child_1, M, V);
        child_2(:,V + 1: M + V) = evaluate_objective(child_2, M, V);
        
        is_crossed(i) = 1;        
        crossed_1(i,:) = child_1(1,1 : M + V);
        crossed_2(i,:) = child_2(1,1 : M + V);
    % With 10 % probability perform mutation. Mutation is based on
    % polynomial mutation. 
    else
        % Select at random the parent.
        parent_3 = round(N*rand(1));
        if parent_3 < 1
            parent_3 = 1;
        end
        % Get the chromosome information for the randomnly selected parent.
        child_3 = parent_chromosome(parent_3,:);
        % Perform mutation on eact element of the selected parent.
    
        for j = 1 : V
            if rand(1) < 0.01
                if j <= SIMULATION_LENGTH
                    child_3(j) = mod(child_3(j) + rand(1)*MAX_HVAC, MAX_HVAC);
                else
                    child_3(j) = rand(1)*MAX_SERVERS;
                end
            end
        end
                
        % Evaluate the objective function for the offspring and as before
        % concatenate the offspring chromosome with objective value.    
        child_3(:,V + 1: M + V) = evaluate_objective(child_3, M, V);
        
        is_mutated(i) = 1;
        mutated(i,:) = child_3(1,1 : M + V);
    end
end

child = zeros(sum(is_crossed)*2+sum(is_mutated),M + V);
p = 1;

for i = 1:N
    if is_crossed(i) == 1
        child(p,:) = crossed_1(1,1 : M + V);
        child(p+1,:) = crossed_2(1,1 : M + V);
        p = p + 2;
    elseif is_mutated(i) == 1
        child(p,:) = mutated(1,1 : M + V);
        p = p + 1;
    end
end

f = child;
