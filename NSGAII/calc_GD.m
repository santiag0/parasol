x = dir('scenarios/workloads/w*');
pow = ['p4';'p3';'p2'];
sch = ['BFH';'EFT';'BSD'];

no_files = size(x);
for j=1:no_files(1,1)
    % disp(x(j).name)
    fw = strsplit(x(j).name,'workload_');
    name_inst = strsplit(fw{2},'.log');
    for s=1:3
        scheduler_name = sch(s,:);
        for p=1:3
            pow_prof = pow(p,:);
            file_sols = sprintf('analisis_experimental/results/%s/%s/%s/log.%s.%s.%s.(75-500).FP.global',scheduler_name,pow_prof,name_inst{1},name_inst{1},pow_prof,scheduler_name);
            file_fp_global = sprintf('analisis_experimental/results/global_pareto_fronts/%s/%s.global.FP',pow_prof,name_inst{1});

            sols = load(file_sols,'-ascii');
            solsfp = load(file_fp_global,'-ascii');
        
            g = GD(sols,solsfp);
            nfs = strsplit(file_sols,'log.');
            name_fs = strsplit(nfs{2},'.(75');
            out = sprintf('%s %.1f',name_fs{1},g);
            disp(out)
        end
    end
end     