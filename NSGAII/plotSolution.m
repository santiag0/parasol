function f = plotSolution(solNumber,sol_file,workload_file,powerRef_file,text_title,file_fig,fid);

% plotSolotion: Summary of this function goes here
%   Detailed explanation goes here

f = [];
global MAX_SERVERS; 
global MAX_HVAC;    
global SIMULATION_LENGH;
global NUMBER_DECISION; 
global TEMP_REF; global POWER_REF;
global TREND; 
global ARX_MODEL;
    
initialise_global_constants(workload_file,powerRef_file);

solution = load(sol_file);
individual = solution(solNumber,1:NUMBER_DECISION);
global DATA_FOR_PLOTTING;
fval = evaluate_objective(individual, 3, NUMBER_DECISION); 
    
h=figure('Name',text_title); 
set(h, 'Visible', 'off');

subplot(3,1,1)
plot(DATA_FOR_PLOTTING.power,'k'); hold on;
plot(POWER_REF, '-.');
h_legend = legend('actual power', 'reference power');
set(h_legend,'FontSize',9);

subplot(3,1,2);
plot(DATA_FOR_PLOTTING.coolingPower,'r'); hold on;
plot(DATA_FOR_PLOTTING.serversPower);
h_legend = legend('cooling power', 'server power');
set(h_legend,'FontSize',9);

subplot(3,1,3);
plot(DATA_FOR_PLOTTING.y+TREND.OutputOffset,'k'); hold on;
plot(TEMP_REF, '-.');
h_legend = legend('actual temperature', 'reference temperature');
set(h_legend,'FontSize',9);

%subplot(5,1,4);
%plot(DATA_FOR_PLOTTING.tv,'k');
%%h_legend = legend('sum of deadline violations (time units)');
%%set(h_legend,'FontSize',9);
%title('sum of deadline violations (time units');

%subplot(5,1,5);
%plot(DATA_FOR_PLOTTING.nv,'k');
%%h_legend = legend('number of tasks with deadline violations');
%%set(h_legend,'FontSize',9);
%title('number of tasks with deadline violations');

set(h, 'Visible', 'on');
FF = sprintf('%s-%s.fig',file_fig,text_title);
saveas(h,FF,'fig');
% Save PNG: now OFF
% FF = sprintf('%s-%s.png',file_fig,text_title);
% saveas(h,FF,'png');
set(h, 'Visible', 'off');

% subplot(4,1,4);
% plot(data.u);
% ylim([-20,20]);
% legend('u1', 'u2', 'u3', 'u4', 'u5', 'u6');
% title(sprintf('fitness = %.2f', data.fitness));

fprintf(fid,'%.2f %.2f %.2f %d ',fval(1),fval(2),fval(3),DATA_FOR_PLOTTING.nv);
