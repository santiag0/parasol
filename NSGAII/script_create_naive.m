global TEMP_REF;
global POWER_REF;
global TREND;
global ARX_MODEL;
global SCHEDULER_ID;
global WORKLOAD;
global MAX_SERVERS;
global DATA_FOR_PLOTTING;

wl = dir('scenarios/workloads_red/workload_hi*');
%wl = dir('scenarios/workloads_red/workload_med*');
%wl = dir('scenarios/temporal/w*');

no_files = size(wl);
for j=1:no_files(1,1)
    disp(wl(j).name)
    
    workload = strcat('scenarios/workloads_red/', wl(j).name);
    %power_ref_file = 'scenarios/powerProfiles/p4.txt';
    %power_ref_file = 'scenarios/powerProfiles/p3.txt';
    power_ref_file = 'scenarios/powerProfiles/p2.txt';
    
    %green_ref_file = 'scenarios/powerProfiles/g1.mat';
    %green_ref_file = 'scenarios/powerProfiles/g2.mat'
    green_ref_file = 'scenarios/powerProfiles/g3.mat'
    
    initialise_global_constants(workload, power_ref_file, green_ref_file);

    n = 150;
    sim_period = n;
    Fan = repmat(0,1,n);
    Servers = repmat(MAX_SERVERS,1,n);
    SCHEDULER_ID = 2;
    [assign,mak,n_violations,t_violations,server_power,off_sim] = scheduler(workload, Servers, sim_period*2, 0);   
    Servers = server_power;

    hysteresis = 1.5;
    Setpoint = ones(150,1) .* 27; %TEMP_REF';
    T14 = repmat(25,1,sim_period);

    meanSP = Setpoint; 
    meanFan = Fan; 
    meanServers = Servers; 
    meanT14 = T14; 
    fromServers = 1; fromFan = 1; fromSP = 1; fromT14 = 1; to = 2;

    AC = 0;
    for i=1:n
        temperature = simulate_n(AC, i, Servers);
        AC_next = AC(i);
        if temperature(i) < Setpoint(i) - (hysteresis/2)
            AC_next = 0;
        end
        if temperature(i) > Setpoint(i) + (hysteresis/2)
            AC_next = 1;
        end
        AC = [AC AC_next];
    end

    x = AC;
    x(x==1) = 150;
    x(x==0) = 250;
    x = [x Servers];
    
    fitness = evaluate_objective(x, 3, n*2);
    
    %x = [x fitness];
    
    [pathstrWL,nameWL,extWL] = fileparts(workload);
    [pathstrPP,namePP,extPP] = fileparts(power_ref_file);
    [pathstrGP,nameGP,extGP] = fileparts(green_ref_file);
    
    %save(strcat('naive-solutions/solNaive-',nameWL,'-',namePP,'.txt'), 'x', '-ASCII');
    save(strcat('naive-solutions/solNaive-',nameGP,'-',nameWL,'-',namePP,'.txt'), 'fitness', '-ASCII');
end
