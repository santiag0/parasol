% find number of non-dominated points 

% non-deferrable workloads
% D = rdir('**\*log*L.p*FP');
D = rdir('**\*log*L.p*FP.global');
% deferrable workloads
% D = rdir('**\*log*-def*FP');
% D = rdir('**\*log*-def*FP.global');

for ii=1:length(D)
    % disp(D(ii).name);
    fp = load(D(ii).name,'-ascii');
    % Best solutions for each objective
    nf = strsplit(D(ii).name,'log.');
    inst_name = strsplit(nf{2},'.p');
    str_log = sprintf('%s ',inst_name{1});
    str_log = sprintf('%s %d',str_log,numel(fp(:,1)));
    disp(str_log);
end;