function sol = anneal_init_simple(tope, MACH_HOLES)
    global WORKLOAD;
    global MAX_NRO_SERVERS;

    curr_holes = MACH_HOLES;
    curr_tope = tope;
    
    task = 0;
    server = 1;
    assign = zeros(MAX_NRO_SERVERS,1);
    assign_tope = ones(MAX_NRO_SERVERS,1);
    
    while task < size(WORKLOAD,1)
        task = task + 1;

        scheduled = 0;
        task_start = WORKLOAD(task,1);
        task_length = WORKLOAD(task,2);
        
        while server <= MAX_NRO_SERVERS && scheduled == 0
            hole_idx = 1;
            
            while hole_idx < curr_tope(server) && scheduled == 0
                hole_start = curr_holes(server,hole_idx);
                hole_end = curr_holes(server,hole_idx+1);
                
                if (task_start < hole_end) && (task_length <= min(hole_end-task_start,hole_end-hole_start))
                    if task_start <= hole_start
                        % Lower bound of the hole
                        curr_holes(server,hole_idx) = hole_start + task_length;
                    elseif task_start + task_length == hole_end
                        % Upper bound of the hole
                        curr_holes(server,hole_idx+1) = hole_end - task_length;
                    else                           
                        aux_holes = curr_holes(server,:);
                        curr_holes(server,hole_idx+1) = task_start;
                        curr_holes(server,hole_idx+2) = task_start+task_length;
                        curr_holes(server,hole_idx+3) = aux_holes(hole_idx+1);

                        for i = hole_idx+2:curr_tope(server)
                            curr_holes(server,i+2) = aux_holes(i);
                        end

                        curr_tope(server) = curr_tope(server)+2;
                    end
                    
                    assign(server,assign_tope(server)) = task;
                    assign_tope(server) = assign_tope(server)+1;

%                     val0 = curr_holes(server,:);
%                     val1 = val0(find(val0,1,'first'):find(val0,1,'last'));
%                     val2 = sort(val1);
% 
%                     display(val1);
%                     display(val2);
%                     display(isequal(val1,val2));
%                     display('============================');
% 
%                     if isequal(val1,val2)
%                         display('ok');
%                     else
%                         assert(isequal(val1,val2));
%                     end

                    scheduled = 1;
                end
                
                hole_idx = hole_idx + 2;
            end
            
            if scheduled == 0
                % No feasible placement in this server.
                server = server + 1;
            end
        end
        
        if scheduled == 0
            % No feasible placement found. Assigning to first server.
            assign(1,assign_tope(1)) = task;
            assign_tope(1) = assign_tope(1)+1;
        end
    end
    
    sol = [];
    for i = 1:server
        sol = horzcat(sol,assign(i,1:assign_tope(i)-1));

        if i < server
            sol = horzcat(sol,size(WORKLOAD,1)+i);
        end
    end

    sol = horzcat(sol,size(WORKLOAD,1)+server:1:size(WORKLOAD,1)+MAX_NRO_SERVERS-1);
end

