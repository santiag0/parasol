function nsga_2(pop,gen,file_workload,file_power_ref,scheduler_id,nro_ejec,green_power)

%% function nsga_2(pop,gen)
% is a multi-objective optimization function where the input arguments are 
% pop - Population size
% gen - Total number of generations
% 
% This functions is based on evolutionary algorithm for finding the optimal
% solution for multiple objective i.e. pareto front for the objectives. 
% Initially enter only the population size and the stoping criteria or
% the total number of generations after which the algorithm will
% automatically stopped. 
%
% You will be asked to enter the number of objective functions, the number
% of decision variables and the range space for the decision variables.
% Also you will have to define your own objective funciton by editing the
% evaluate_objective() function. A sample objective function is described
% in evaluate_objective.m. Kindly make sure that the objective function
% which you define match the number of objectives that you have entered as
% well as the number of decision variables that you have entered. The
% decision variable space is continuous for this function, but the
% objective space may or may not be continuous.
%
% Original algorithm NSGA-II was developed by researchers in Kanpur Genetic
% Algorithm Labarotary and kindly visit their website for more information
% http://www.iitk.ac.in/kangal/


%  Copyright (c) 2009, Aravind Seshadri
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without 
%  modification, are permitted provided that the following conditions are 
%  met:
%
%     * Redistributions of source code must retain the above copyright 
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright 
%       notice, this list of conditions and the following disclaimer in 
%       the documentation and/or other materials provided with the distribution
%      
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
%  POSSIBILITY OF SUCH DAMAGE.

%% Simple error checking
% Number of Arguments
% Check for the number of arguments. The two input arguments are necessary
% to run this function.
if nargin < 5
    error('Use: nsga_2 <population size> <number of generations> <workload file> <power reference file> <scheduler_id>.');
end
% Both the input arguments need to of integer data type
if isnumeric(pop) == 0 || isnumeric(gen) == 0
    error('Both input arguments pop and gen should be integer datatype');
end
% Check for workload and power reference files
if exist(file_workload,'file') == 0
    error('Workload file not found !');
end
if exist(file_power_ref,'file') == 0
    error('Power reference file not found !');
end
% Minimum population size has to be 20 individuals
if pop < 20
    error('Minimum population for running this function is 20');
end
if gen < 5
    error('Minimum number of generations is 5');
end
% Make sure pop and gen are integers
pop = round(pop);
gen = round(gen);
%% Objective Function
% The objective function description contains information about the
% objective function. M is the dimension of the objective space, V is the
% dimension of decision variable space, min_range and max_range are the
% range for the variables in the decision variable space. User has to
% define the objective functions using the decision variables. Make sure to
% edit the function 'evaluate_objective' to suit your needs.

global WORKLOAD;

global MAX_SERVERS;
global MAX_HVAC;
global SIMULATION_LENGTH;
global NUMBER_DECISION;

global SCHEDULER_ID;
SCHEDULER_ID = scheduler_id;

global GEN_NUM;
GEN_NUM = 0;

% hold off;

initialise_global_constants(file_workload,file_power_ref,green_power);

[M, V, min_range, max_range] = objective_description_function();

%% Initialize the population
% Population is initialized with random values which are within the
% specified range. Each chromosome consists of the decision variables. Also
% the value of the objective functions, rank and crowding distance
% information is also added to the chromosome vector but only the elements
% of the vector which has the decision variables are operated upon to
% perform the genetic operations like corssover and mutation.
chromosome = initialize_variables(pop, M, V, min_range, max_range);

%% Sort the initialized population
% Sort the population using non-domination-sort. This returns two columns
% for each individual which are the rank and the crowding distance
% corresponding to their position in the front they belong. At this stage
% the rank and the crowding distance for each chromosome is added to the
% chromosome vector for easy of computation.
chromosome = non_domination_sort_mod(chromosome, M, V);

%% Start the evolution process
% The following are performed in each generation
% * Select the parents which are fit for reproduction
% * Perfrom crossover and Mutation operator on the selected parents
% * Perform Selection from the parents and the offsprings
% * Replace the unfit individuals with the fit individuals to maintain a
%   constant population size.

% DEBUG
% printPareto(chromosome(:,V+1),chromosome(:,V+2),chromosome(:,V+3),1);

display(['> Inicio NSGA-II: ', datestr(now)]);

for i = 1 : gen
    GEN_NUM = i;
    
    %display('========================================');
    %display(['Time: ', datestr(now)]);
    %display(i);

    % Select the parents
    % Parents are selected for reproduction to generate offspring. The
    % original NSGA-II uses a binary tournament selection based on the
    % crowded-comparision operator. The arguments are 
    % pool - size of the mating pool. It is common to have this to be half the
    %        population size.
    % tour - Tournament size. Original NSGA-II uses a binary tournament
    %        selection, but to see the effect of tournament size this is kept
    %        arbitary, to be choosen by the user.
    
    
    % DEBUG
    %if (i == gen/2)
    %    printPareto(chromosome(:,V+1),chromosome(:,V+2),chromosome(:,V+3),i);
    %end    
    
    pool = round(pop/2);
    tour = 2;
    % Selection process
    % A binary tournament selection is employed in NSGA-II. In a binary
    % tournament selection process two individuals are selected at random
    % and their fitness is compared. The individual with better fitness is
    % selcted as a parent. Tournament selection is carried out until the
    % pool size is filled. Basically a pool size is the number of parents
    % to be selected. The input arguments to the function
    % tournament_selection are chromosome, pool, tour. The function uses
    % only the information from last two elements in the chromosome vector.
    % The last element has the crowding distance information while the
    % penultimate element has the rank information. Selection is based on
    % rank and if individuals with same rank are encountered, crowding
    % distance is compared. A lower rank and higher crowding distance is
    % the selection criteria.
    parent_chromosome = tournament_selection(chromosome, pool, tour);

    % Perfrom crossover and Mutation operator
    % The original NSGA-II algorithm uses Simulated Binary Crossover (SBX) and
    % Polynomial  mutation. Crossover probability pc = 0.9 and mutation
    % probability is pm = 1/n, where n is the number of decision variables.
    % Both real-coded GA and binary-coded GA are implemented in the original
    % algorithm, while in this program only the real-coded GA is considered.
    % The distribution indeices for crossover and mutation operators as mu = 20
    % and mum = 20 respectively.
    mu = 20;
    mum = 20;
    offspring_chromosome = ...
        genetic_operator(parent_chromosome, ...
        M, V, mu, mum, min_range, max_range);
    
     %offspring_chromosome = ...
     %   genetic_operator_par(parent_chromosome, ...
     %   M, V, mu, mum, min_range, max_range);

    % Intermediate population
    % Intermediate population is the combined population of parents and
    % offsprings of the current generation. The population size is two
    % times the initial population.
    
    [main_pop,temp] = size(chromosome);
    [offspring_pop,temp] = size(offspring_chromosome);
    % temp is a dummy variable.
    clear temp
    % intermediate_chromosome is a concatenation of current population and
    % the offspring population.
    intermediate_chromosome(1:main_pop,:) = chromosome;
    intermediate_chromosome(main_pop + 1 : main_pop + offspring_pop,1 : M+V) = ...
        offspring_chromosome;

    % Non-domination-sort of intermediate population
    % The intermediate population is sorted again based on non-domination sort
    % before the replacement operator is performed on the intermediate
    % population.
    intermediate_chromosome = ...
        non_domination_sort_mod(intermediate_chromosome, M, V);
    % Perform Selection
    % Once the intermediate population is sorted only the best solution is
    % selected based on it rank and crowding distance. Each front is filled in
    % ascending order until the addition of population size is reached. The
    % last front is included in the population based on the individuals with
    % least crowding distance
    chromosome = replace_chromosome(intermediate_chromosome, M, V, pop);
    
    if ~mod(i,100)
        %clc
        fprintf('%d generations completed\n',i);
    end
end

display(['> Fin NSGA-II: ', datestr(now)]);

%% Visualize
% The following is used to visualize the result if objective space
% dimension is visualizable.
%if M == 2
%    plot(chromosome(:,V + 1),chromosome(:,V + 2),'*');
%elseif M == 3
%    plot3(chromosome(:,V + 1),chromosome(:,V + 2),chromosome(:,V + 3),'*');
%    xlabel('f(1) - energy profile');
%    ylabel('f(2) - temperature profile');
%    zlabel('f(3) - QoS');
%    grid on
%    axis square
%end

%fw = strsplit(file_workload,'workload_');
fw = strsplit(file_workload,'_');
fp = strsplit(file_power_ref,'powerProfiles/');
fgp = strsplit(green_power,'powerProfiles/');

switch scheduler_id
    case 1
        scheduler_name = 'BFH';
    case 2
        scheduler_name = 'EFT';
    case 3
        scheduler_name = 'BSD';
    case 4
        scheduler_name = 'SA';
end

%name_inst = strsplit(fw{2},'.log');
name_inst = fw{2};
num_inst = fw{6};
logging_dir = sprintf('analisis_experimental/results/%s/%s/%s/%s/%s',scheduler_name,fgp{2},fp{2},name_inst,num_inst)

mkdir(logging_dir);

% Result
% Save the result in ASCII text format.
file_sol = sprintf('%s/solution.%s.%s.%s.(%d-%d).%d.sol',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec)
save(file_sol,'chromosome','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec)
file_fig = sprintf('%s/fig.%s.%s.%s.(%d-%d).%d',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec)
printPareto(chromosome(:,V+1),chromosome(:,V+2),chromosome(:,V+3),0,SCHEDULER_ID,file_workload,file_power_ref,file_sol,file_log,file_fig);

SCHEDULER_ID = 4;

global SA_GEN_START;
global SA_PROB;
global SA_VERSION;
global SA_ITERS;

SA_GEN_START = -1;
SA_PROB = 1;
SA_VERSION = 1;
SA_ITERS = 5;

o_results = zeros(20,3);
n_results = zeros(20,3);
improvs = zeros(20,3);
diffs = zeros(20,3);

total_nd = 0;

display(['> Inicio SA: ', datestr(now)]);

for i = 1:20
    nondom = 1;

    for h = 1:20
        if (chromosome(h, 301) < chromosome(i, 301) && ...
                chromosome(h, 302) < chromosome(i, 302) && ...
                chromosome(h, 303) < chromosome(i, 303))

            nondom = 0;
        end
    end

    if nondom == 1    
        %display(i);
        %display(datestr(now));

        total_nd = total_nd + 1;

        o_results(total_nd,1) = chromosome(i, 301);
        o_results(total_nd,2) = chromosome(i, 302);
        o_results(total_nd,3) = chromosome(i, 303);

        f = evaluate_objective(chromosome(i,:), M, V);

        if o_results(total_nd,1) >= f(1) && o_results(total_nd,2) >= f(2) && o_results(total_nd,2) >= f(2)
            new_min_power = f(1);
            new_min_budget = f(2);
            new_min_viol = f(3);
        else
            new_min_power = o_results(total_nd,1);
            new_min_budget = o_results(total_nd,2);
            new_min_viol = o_results(total_nd,3);
        end

        n_results(total_nd,1) = new_min_power;
        n_results(total_nd,2) = new_min_budget;
        n_results(total_nd,3) = new_min_viol;

        diffs(total_nd,1) = chromosome(i, 301) - new_min_power;
        diffs(total_nd,2) = chromosome(i, 302) - new_min_budget;
        diffs(total_nd,3) = chromosome(i, 303) - new_min_viol;

        if chromosome(i, 301) == 0
            improvs(total_nd,1) = 0;
        else
            improvs(total_nd,1) = 1 - new_min_power / chromosome(i, 301);
        end

        improvs(total_nd,2) = 1 - new_min_budget / chromosome(i, 302);

        if chromosome(total_nd, 303) == 0
            improvs(total_nd,3) = 0;
        else
            improvs(total_nd,3) = 1 - new_min_viol / chromosome(i, 303);
        end  
    end
end

display(['> Fin SA: ', datestr(now)]);

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.nFP.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec);
save(file_log,'n_results','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.diff.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec);
save(file_log,'diffs','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).%d.impr.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen,nro_ejec);
save(file_log,'improvs','-ascii','-append');

logging_dir = sprintf('analisis_experimental/results/%s',scheduler_name);

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).means.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen);
means = [mean(improvs(1:total_nd,1)) mean(improvs(1:total_nd,2)) mean(improvs(1:total_nd,3))];
save(file_log,'means','-ascii','-append');

file_log = sprintf('%s/log.%s.%s.%s.(%d-%d).stdev.log',logging_dir,name_inst,fp{2},scheduler_name,pop,gen);
stdev = [std(improvs(1:total_nd,1)) std(improvs(1:total_nd,2)) std(improvs(1:total_nd,3))];
save(file_log,'stdev','-ascii','-append');

%file_fig = sprintf('%s/fig.%s.%s.%s.(%d-%d).%d',logging_dir,name_inst{1},fp{2},scheduler_name,pop,gen,nro_ejec)
%%printPareto(chromosome(:,V+1),chromosome(:,V+2),chromosome(:,V+3),0,SCHEDULER_ID,file_workload,file_power_ref,file_sol,file_log,file_fig);
%printPareto(true_fit(:,1),true_fit(:,2),true_fit(:,3),0,SCHEDULER_ID,file_workload,file_power_ref,file_sol,file_log,file_fig);

% figure
% printPareto2(chromosome(:,V+1),chromosome(:,V+2),chromosome(:,V+3),0,SCHEDULER_ID);

%hold on;