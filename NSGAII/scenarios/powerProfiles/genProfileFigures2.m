n = 150;
MAX_PWR = 4220;

p1 = zeros(n,1);
p1(1:n/3) = 0.2;
p1(n/3+1:2*n/3) = 0.5;
p1(2*n/3+1:n) = 0.8;

%C
p2 = zeros(n,1);
p2(1:n/3) = 0.8;
p2(n/3+1:2*n/3) = linspace(0.8, 0.2, n/3);
p2(2*n/3+1:n) = 0.2;

%B
p3 = zeros(n,1);
p3(1:30) = 0.5;
p3(31:50) = 0.8;
p3(51:90) = 0.2;
p3(91:110) = 0.8;
p3(111:150) = 0.5;

%A
p4 = zeros(n,1);
p4(1:n/3) = 0.2;
p4(n/3+1:2*n/3) = 0.8;
p4(2*n/3+1:n) = 0.2;

p1 = p1*MAX_PWR;
p2 = p2*MAX_PWR;
p3 = p3*MAX_PWR;
p4 = p4*MAX_PWR;

hfig = figure % new figure
%ax1 = subplot(3,1,1); % top subplot
%ax2 = subplot(3,1,2); % bottom subplot
%ax3 = subplot(3,1,3); % bottom subplot
set(hfig, 'Position', [0 0 500 250])

x = [1:150];
y1 = p4;
y2 = p3;
y3 = p2;

area(x,y1)
% title(ax2,'Profile A')

 
%area(x,y2)
% title(ax2,'Profile B')

 
%area(x,y3)
% title(ax3,'Profile C')

ylabel('Energy (kW)')
xlabel('Timestep')

set(findobj('type','axes'),'fontsize',22) 
xlhand = get(gca,'xlabel')
set(xlhand,'fontsize',22) 
ylhand = get(gca,'ylabel')
set(ylhand,'fontsize',22) 