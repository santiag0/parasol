run greenPowerProfiles

hfig = figure % new figure
%ax1 = subplot(2,1,1); % top subplot
%ax2 = subplot(2,1,2); % bottom subplot

set(hfig, 'Position', [0 0 500 250])

x = [1:150];
y1 = GREEN_GENERATION_G1;
y2 = GREEN_GENERATION_G2;

b = area(x,y1);
%title('Early morning energy generation');

%figure

%b = area(x,y2)
%title(ax2,'Midday energy generation')

ylabel('Energy (kW)')
xlabel('Timestep')
axis([-inf,150,-inf,500])

set(b, 'FaceColor', 'green');
set(findobj('type','axes'),'fontsize',22) 
xlhand = get(gca,'xlabel')
set(xlhand,'fontsize',22) 
ylhand = get(gca,'ylabel')
set(ylhand,'fontsize',22) 