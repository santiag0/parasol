function findPareto_from3(file_workload,pow_prof)

% Fing Pareto front from three files of nondominated solutions

fw = strsplit(file_workload,'workload_');
name_inst = strsplit(fw{2},'.log');

name_inst;

file_sols1 = sprintf('analisis_experimental/results/BFH/%s/%s/log.%s.%s.BFH.(75-500).FP',pow_prof,name_inst{1},name_inst{1},pow_prof);
file_sols2 = sprintf('analisis_experimental/results/EFT/%s/%s/log.%s.%s.EFT.(75-500).FP',pow_prof,name_inst{1},name_inst{1},pow_prof);
file_sols3 = sprintf('analisis_experimental/results/BSD/%s/%s/log.%s.%s.BSD.(75-500).FP',pow_prof,name_inst{1},name_inst{1},pow_prof);

sols1 = load(file_sols1,'-ascii');
ip1 = true(numel(sols1(:,1)),1);

sols2 = load(file_sols2,'-ascii');
ip2 = true(numel(sols2(:,1)),1);

sols3 = load(file_sols3,'-ascii');
ip3 = true(numel(sols3(:,1)),1);

for k=1:numel(sols1(:,1))
    for h=1:numel(sols1(:,1))
        if (sols1(h,1)<sols1(k,1) & sols1(h,2)<sols1(k,2) & sols1(h,3)<sols1(k,3))
            ip1(k) = false;
        end
    end
    for h=1:numel(sols2(:,1))
        if (sols2(h,1)<sols1(k,1) & sols2(h,2)<sols1(k,2) & sols2(h,3)<sols1(k,3))
            ip1(k) = false;
        end
    end
    for h=1:numel(sols3(:,1))
        if (sols3(h,1)<sols1(k,1) & sols3(h,2)<sols1(k,2) & sols3(h,3)<sols1(k,3))
            ip1(k) = false;
        end
    end
end

for k=1:numel(sols2(:,1))
    for h=1:numel(sols1(:,1))
        if (sols1(h,1)<sols2(k,1) & sols1(h,2)<sols2(k,2) & sols1(h,3)<sols2(k,3))
            ip2(k) = false;
        end
    end
    for h=1:numel(sols2(:,1))
        if (sols2(h,1)<sols2(k,1) & sols2(h,2)<sols2(k,2) & sols2(h,3)<sols2(k,3))
            ip2(k) = false;
        end
    end
    for h=1:numel(sols3(:,1))
        if (sols3(h,1)<sols2(k,1) & sols3(h,2)<sols2(k,2) & sols3(h,3)<sols2(k,3))
            ip2(k) = false;
        end
    end
end

for k=1:numel(sols3(:,1))
    for h=1:numel(sols1(:,1))
        if (sols1(h,1)<sols3(k,1) & sols1(h,2)<sols3(k,2) & sols1(h,3)<sols3(k,3))
            ip3(k) = false;
        end
    end
    for h=1:numel(sols2(:,1))
        if (sols2(h,1)<sols3(k,1) & sols2(h,2)<sols3(k,2) & sols2(h,3)<sols3(k,3))
            ip3(k) = false;
        end
    end
    for h=1:numel(sols3(:,1))
        if (sols3(h,1)<sols3(k,1) & sols3(h,2)<sols3(k,2) & sols3(h,3)<sols3(k,3))
            ip3(k) = false;
        end
    end
end

name_FP_global = sprintf('analisis_experimental/results/global_pareto_fronts/%s./%s.global.FP',pow_prof,name_inst{1});

sP = sols1(ip1,:);
save(name_FP_global,'sP','-ascii','-append');

sP = sols2(ip2,:);
save(name_FP_global,'sP','-ascii','-append');

sP = sols3(ip3,:);
save(name_FP_global,'sP','-ascii','-append');

