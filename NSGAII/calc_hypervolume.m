x = dir('scenarios/workloads/w*');
no_files = size(x);
pow = ['p4';'p3';'p2'];
sch = ['BFH';'EFT';'BSD'];
N = 100000 % number of points for Monte Carlo
for j=1:no_files(1,1)
    % disp(x(j).name)
    fw = strsplit(x(j).name,'workload_');
    name_inst = strsplit(fw{2},'.log');
    for s=1:3
        scheduler_name = sch(s,:);
        for p=1:3
            pow_prof = pow(p,:);
            file_sols = sprintf('analisis_experimental/results/%s/%s/%s/log.%s.%s.%s.(75-500).FP.global',scheduler_name,pow_prof,name_inst{1},name_inst{1},pow_prof,scheduler_name);
            file_fp_global = sprintf('analisis_experimental/results/global_pareto_fronts/%s/%s.global.FP',pow_prof,name_inst{1});

            sols = load(file_sols,'-ascii');
            solsfp = load(file_fp_global,'-ascii');
        
            % find reference point: point in FP global with maximum value
            % for each objective (Pareto front extremes)
            [sf1,ind1] = sort(sols(:,1),'descend');
            ef1 = sols(ind1(1),1);
            [sf2,ind2] = sort(sols(:,2),'descend');
            ef2 = sols(ind2(1),2);
            [sf3,ind3] = sort(sols(:,3),'descend');
            ef3 = sols(ind3(1),3);
            r = [ef1 ef2 ef3];
            
            hv1 = hypervolume(sols,r,N);
            hvref = hypervolume(solsfp,r,N);
            
            hv = hv1/hvref;
            
            n_fs = strsplit(file_sols,'results/');
            name_fs = strsplit(n_fs{2},'.(75');
            out = sprintf('%s %.2f %.2f %.2f',name_fs{1},hv1,hvref,hv);
            disp(out)
            % pause(2)
        end
    end
end  