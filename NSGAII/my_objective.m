function ret = my_objective(x, tempRef, powerRef)
timeStart = cputime;


sim_period = size(x,1)/2;


%smoothing meanSP
windowFan = 21;
windowSP = 400;
windowServers = 90;
windowT14 = 210;

%display(sprintf('(1) Elapsed: %.2f', cputime-timeStart));

Setpoint = tempRef';%repmat(30,1,sim_period);
%Fan = repmat(0,1,sim_period);
%Servers = repmat(1800,1,sim_period);
T14 = repmat(25,1,sim_period);

%0 <=x(i) <= 100 means that the NV is on and the fan runs at x(i) speed (%)
%100 <x(i) <= 200 means that the AC is on and the Fan is off
%200 <x(i) <= 300 means that both AC and Fan are off

cooling = x(1:round(sim_period));
Servers = x(round(sim_period)+1:end)';

AC = (cooling>100 & cooling<200)';
Fan = (cooling.*(cooling<=100))';


Damper = Fan > 0;

%display(sprintf('(2) Elapsed: %.2f', cputime-timeStart));

meanSP = Setpoint; 
meanFan = Fan; 
meanServers = Servers; 
meanT14 = T14; 
meanAC = AC; 
fromServers = 1; fromFan = 1; fromSP = 1; fromT14 = 1; to = 2;
for i=2:sim_period
%    meanSP2(i) = mean(Setpoint(fromSP:to));
% A MUCH MOR EFFICIENT WAY TO COMPUTE THE MOVING AVERAGE:
     leavingSP = 0;
     correctionSP = 1;
     if fromSP > 1
         leavingSP = Setpoint(fromSP-1)/(to-fromSP+1);
     else
         correctionSP = (to-fromSP) / (to-fromSP+1);
     end
    meanSP(i) = correctionSP*meanSP(i-1) + Setpoint(i)/(to-fromSP+1) - leavingSP;

%    meanT14(i) = mean(T14(fromT14:to));
% A MUCH MOR EFFICIENT WAY TO COMPUTE THE MOVING AVERAGE:
    leavingT14 = 0;
     correctionT14 = 1;
     if fromT14 > 1
         leavingT14 = T14(fromT14-1)/(to-fromT14+1);
     else
         correctionT14 = (to-fromT14) / (to-fromT14+1);
     end
    meanT14(i) = correctionT14*meanT14(i-1) + T14(i)/(to-fromT14+1) - leavingT14;

    
    %display(sprintf('Entering: %.2f, Leaving: %.2f', Setpoint(i)/(to-fromSP+1), leavingSP));    
    meanFan(i) = mean(Fan(fromFan:to));
    meanServers(i) = mean(Servers(fromServers:to));

    
    to = to + 1;
    fromSP = max(1,to-windowSP+1);
    fromServers = max(1,to-windowServers+1);
    fromFan = max(1,to-windowFan+1);
    fromT14 = max(1,to-windowT14+1);
end

%figure(); plot(meanT14); hold on; plot(meanT142, 'r'); hold off;


%display(sprintf('MeanSP: %.2f, MeanT14:%.2f\n', meanSP, meanT14));

%figure; plot(meanT14); hold on; plot(meanSP, 'g'); hold off;

U2 = Damper.*(((1-AC).*meanFan).^(1/1)).*abs(meanSP-meanT14).^(1/9).*sign(meanSP-meanT14);



U2 = max(U2,0) - 18.6571;


%display(sprintf('(4) Elapsed: %.2f', cputime-timeStart));
Tr = load('Tr.mat');
Tr = Tr.Tr;
arx_model = load('arx_model.mat');
%display(sprintf('(4.1) Elapsed: %.2f', cputime-timeStart));
arx_model = arx_model.arx_model;

U = [meanAC; U2;  T14; meanServers; Damper; meanSP]';
for i = 1:size(U,2)
    U(:,i) = U(:,i)-Tr.InputOffset(i);
end

%display(sprintf('(5) Elapsed: %.2f', cputime-timeStart));
%U = U - repmat(Tr.InputOffset,size(U,1),1);


y = sim(arx_model,U);


coolingPower = AC*2300 + (Fan/100)*410;
power = coolingPower+Servers;

%fitness = mean(abs(power'-powerRef)) + max(abs(power'-powerRef));
fitness = sum((power'-powerRef).^2);

%display(mean(abs(power'-powerRef)));
%display(max(abs(power'-powerRef)));


penalty = sum(max((y+Tr.OutputOffset) - tempRef, 0).^2);
% display(max(((y+Tr.OutputOffset) - tempRef) ,0));
% display(penalty);
% display(y(40)+Tr.OutputOffset);
% display(tempRef(40));

%USE THIS WHEN RUNNING THE GA
%ret = (penalty+1)*fitness;

%USE THIS WHEN PLOTTING THE SOLUTION AFTER RUNNING THE GA
ret = struct('y',y, 'u',U, 'power', coolingPower + Servers, 'coolingPower', coolingPower, 'serversPower', Servers, 'fitness', penalty+fitness);
end