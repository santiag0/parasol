function [current] = anneal_metrics(current)
    sol = current.sol;
    sim_period = current.V/2;

    global MAX_ENERGY_SERVER;
    global IDLE_ENERGY_SERVER;
    global WORKLOAD;
    global MAX_NRO_SERVERS;

    makespan_local = zeros(MAX_NRO_SERVERS,1);
    n_violations = 0;
    t_violations = 0;
    power = zeros(sim_period, 1); %ones(1,150) * IDLE_ENERGY_SERVER * MAX_NRO_SERVERS; %zeros(MAX_NRO_SERVERS);
           
    POWER_DIFF = MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER;

    current.makespan = Inf;   
    current.t_violations = Inf;
    current.n_violations = Inf;
    current.taskPower = power;
    
    for task = 1:size(WORKLOAD,1)
        server = sol.tasks_assign(task);
        sch_start = sol.tasks_start(task);
        
        hole_idx = 1;      
        scheduled = 0;
        
        task_start = WORKLOAD(task,1);
        task_length = WORKLOAD(task,2);
        task_deadline = WORKLOAD(task,3);

        while hole_idx < sol.curr_tope(server) && scheduled == 0
            hole_start = sol.curr_holes(server,hole_idx);
            hole_end = sol.curr_holes(server,hole_idx+1);

            if (hole_start <= sch_start) && (hole_end >= sch_start)            
                if (task_start < hole_end) && (task_length <= min(hole_end-task_start,hole_end-hole_start))
                    for t = max(hole_start,task_start):max(hole_start,task_start)+task_length-1
                        power(t) = power(t) + POWER_DIFF;
                    end

                    if task_start <= hole_start
                        % Lower bound of the hole
                        sol.curr_holes(server,hole_idx) = hole_start + task_length;

                        if sol.curr_holes(server,hole_idx) > task_deadline
                            n_violations = n_violations + 1;
                            t_violations = t_violations + (sol.curr_holes(server,hole_idx) - task_deadline + 1);
                        end

                        if makespan_local(server) < hole_start + task_length - 1
                            makespan_local(server) = hole_start + task_length - 1;
                        end
                    elseif task_start + task_length == hole_end
                        % Upper bound of the hole
                        sol.curr_holes(server,hole_idx+1) = hole_end - task_length;

                        if hole_end >= task_deadline
                            n_violations = n_violations + 1;
                            t_violations = t_violations + (hole_end - task_deadline + 1);
                        end

                        if makespan_local(server) < task_start + task_length - 1
                            makespan_local(server) = task_start + task_length - 1;
                        end

                        hole_idx = hole_idx + 2;
                    else                           
                        aux_holes = sol.curr_holes(server,:);
                        sol.curr_holes(server,hole_idx+1) = task_start;
                        sol.curr_holes(server,hole_idx+2) = task_start+task_length;
                        sol.curr_holes(server,hole_idx+3) = aux_holes(hole_idx+1);

                        for i = hole_idx+2:sol.curr_tope(server)
                            sol.curr_holes(server,i+2) = aux_holes(i);
                        end

                        sol.curr_tope(server) = sol.curr_tope(server)+2;

                        if task_start+task_length > task_deadline
                            n_violations = n_violations + 1;
                            t_violations = t_violations + (task_start+task_length - task_deadline + 1);
                        end

                        if makespan_local(server) < task_start+task_length-1
                            makespan_local(server) = task_start+task_length-1;
                        end

                        hole_idx = hole_idx + 2;
                    end                        

                    scheduled = 1;
                else
                    hole_idx = hole_idx + 2;
                end
            end
        end
            
        if scheduled == 0
            % No feasible solution found. Extending timeline.
            if makespan_local(server) + task_length > task_deadline
                n_violations = n_violations + 1;
                t_violations = t_violations + (makespan_local(server) + task_length - task_deadline);
            end

            makespan_local(server) = makespan_local(server) + task_length - 1;
            return;
        end
    end
    
    current.makespan = max(makespan_local);   
    current.t_violations = t_violations;
    current.n_violations = n_violations;
    current.taskPower = power;
    [current.power_total,current.budget,current.violations] = anneal_objs(current);
end

