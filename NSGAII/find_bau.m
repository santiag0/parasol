for cval=1:300
    load('sample.mat');
    %load('scenarios/powerProfiles/g1.mat');
    %load('scenarios/powerProfiles/g2.mat');
    load('scenarios/powerProfiles/g3.mat');
    
    f = [];
    sim_period = V/2;

    %smoothing meanSP
    windowFan = 21;
    windowSP = 400;
    windowServers = 90;
    windowT14 = 210;

    %display(sprintf('(1) Elapsed: %.2f', cputime-timeStart));

    Setpoint = TEMP_REF';%repmat(30,1,sim_period);
    %Fan = repmat(0,1,sim_period);
    %Servers = repmat(1800,1,sim_period);
    T14 = repmat(25,1,sim_period);

    %0 <=x(i) <= 100 means that the NV is on and the fan runs at x(i) speed (%)
    %100 <x(i) <= 200 means that the AC is on and the Fan is off
    %200 <x(i) <= 300 means that both AC and Fan are off

    %cooling = x(1:round(sim_period));
    cooling = ones(1,150) * cval;
    
    %Servers = x(round(sim_period)+1:V); %the last two elements are the objective values
    Servers = ones(1,150) * 1920;
    
    AC = (cooling>100 & cooling<=200);
    Fan = (cooling.*(cooling<=100));

    Damper = Fan > 0;

    meanSP = Setpoint; 
    meanFan = Fan; 
    meanServers = Servers; 
    meanT14 = T14; 
    meanAC = AC; 
    fromServers = 1; fromFan = 1; fromSP = 1; fromT14 = 1; to = 2;
    for i=2:sim_period
        %    meanSP2(i) = mean(Setpoint(fromSP:to));
        % A MUCH MOR EFFICIENT WAY TO COMPUTE THE MOVING AVERAGE:
        leavingSP = 0;
        correctionSP = 1;
        if fromSP > 1
            leavingSP = Setpoint(fromSP-1)/(to-fromSP+1);
        else
            correctionSP = (to-fromSP) / (to-fromSP+1);
        end
        meanSP(i) = correctionSP*meanSP(i-1) + Setpoint(i)/(to-fromSP+1) - leavingSP;

        %    meanT14(i) = mean(T14(fromT14:to));
        % A MUCH MOR EFFICIENT WAY TO COMPUTE THE MOVING AVERAGE:
        leavingT14 = 0;
        correctionT14 = 1;
        if fromT14 > 1
            leavingT14 = T14(fromT14-1)/(to-fromT14+1);
        else
            correctionT14 = (to-fromT14) / (to-fromT14+1);
        end
        meanT14(i) = correctionT14*meanT14(i-1) + T14(i)/(to-fromT14+1) - leavingT14;


        %display(sprintf('Entering: %.2f, Leaving: %.2f', Setpoint(i)/(to-fromSP+1), leavingSP));    
        meanFan(i) = mean(Fan(fromFan:to));
        meanServers(i) = mean(Servers(fromServers:to));

        to = to + 1;
        fromSP = max(1,to-windowSP+1);
        fromServers = max(1,to-windowServers+1);
        fromFan = max(1,to-windowFan+1);
        fromT14 = max(1,to-windowT14+1);
    end

    U2 = Damper.*(((1-AC).*meanFan).^(1/1)).*abs(meanSP-meanT14).^(1/9).*sign(meanSP-meanT14);
    U2 = max(U2,0) - 18.6571;

    U = [meanAC; U2;  T14; meanServers; Damper; meanSP]';
    for i = 1:size(U,2)
        U(:,i) = U(:,i)-TREND.InputOffset(i);
    end

    %U = U - repmat(Tr.InputOffset,size(U,1),1);

    %PARA DEFINIR LA TEMPERATURA INTERNA INICIAL, CAMBIAR starting_temp
    starting_temp = 25;
    first_temp_change = 0.1;
    det_temp = starting_temp - TREND.OutputOffset;
    initial_conditions = [det_temp; det_temp + ARX_MODEL.A(1,2)*(det_temp-first_temp_change)-(U(1,:)*ARX_MODEL.B(:,2))];
    
    y = sim(ARX_MODEL,U, initial_conditions);

    coolingPower = AC*2300 + (Fan/100)*410;
    % OLD: power not adjusted by the scheduling
    % power = coolingPower+Servers;

    %ServersGreen = rand(1,150) * 1000;
    %ServersBrown = Servers;
    %Servers = ServersBrown + ServersGreen;

    if max(y + TREND.OutputOffset) <= 27
        %disp([TREND.OutputOffset, sum(coolingPower)]);
        disp('Cooling value:');
        disp(cval);
        disp('Cooling power:');
        disp(sum(coolingPower));
        disp('Temperature:');
        disp(max(y + TREND.OutputOffset));
        disp('Budget:');
        budget = ((Servers+coolingPower) * BROWN_ENERGY_BUY);
        disp(budget);
        disp('============================');
    end
    
    %diff_temp = (y + TREND.OutputOffset) - (ones(150,1) .* 27); %TEMP_REF;
    %diff_temp = 0;
end
