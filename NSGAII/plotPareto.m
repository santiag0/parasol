function plotPareto(pareto_file,scheduler_id)

pareto_points = load(pareto_file);
f1 = pareto_points(:,1);
f2 = pareto_points(:,2);
f3 = pareto_points(:,3);

switch scheduler_id
    case 1
        plot3(f1,f2,f3,'g+')
    case 2
        plot3(f1,f2,f3,'r*')
    case 3
        plot3(f1,f2,f3,'b*')
end

xlabel('f(1) - energy profile deviation');
ylabel('f(2) - temperature profile deviation');
zlabel('f(3) - QoS (deadline violations)');
grid on
axis square

% set(h, 'Visible', 'on');
% Save as PNG: now OFF
% FF = sprintf('%s.FP.png',file_fig);
% saveas(h,FF,'png');

% Save as fig: now OFF, to allow plotting different PF in the same figure
% FF = sprintf('%s.fig',pareto_file);
% saveas(h,FF,'fig');
% set(h, 'Visible', 'off');

% Point references in file: now OFF
% f = gcf();
% dcm_obj = datacursormode(f);
% set(dcm_obj,'UpdateFcn',@showDatatips)