x = dir('./f*');
no_files = size(x);
for j=1:no_files(1,1)
    h = openfig(x(j).name,'new');
    
    subplot(3,1,1);
    ylabel('power (W)');
    subplot(3,1,2);
    ylabel('power (W)');
    subplot(3,1,3);
    ylabel('temperature (C)');
    xlabel('time (30 second intervals)')
    legend('Location', 'Best')
    
    FF = sprintf('./output/%s.eps',x(j).name);
    saveas(h,FF,'epsc');
    

end