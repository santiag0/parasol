function sol = anneal_init_efth(tope, MACH_HOLES, best_gap_end, best_gap_end_mach)
    global WORKLOAD;
    global MAX_NRO_SERVERS;
    
    assign = zeros(MAX_NRO_SERVERS,1);
    assign_tope = ones(MAX_NRO_SERVERS,1);
    
    global MAX_ENERGY_SERVER;
    global IDLE_ENERGY_SERVER;
    
    POWER_DIFF = MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER;
    
    assign = zeros(MAX_NRO_SERVERS,1);
    assign_tope = ones(MAX_NRO_SERVERS,1);

    sol.tasks_assign = zeros(size(WORKLOAD,1),1);
    sol.tasks_start = zeros(size(WORKLOAD,1),1);
    sol.curr_holes = MACH_HOLES;
    sol.curr_tope = tope;
    sol.taskPower = zeros(150, 1);
    sol.n_violations = 0;
    sol.t_violations = 0;
    
    %mak_local = zeros(MAX_NRO_SERVERS,1);
    
    % find best_end_gap_mach
    best_gap_end = -Inf;
    for j=1:MAX_NRO_SERVERS
        if (sol.curr_holes(j,sol.curr_tope(j))-sol.curr_holes(j,sol.curr_tope(j)-1) > best_gap_end)
            best_gap_end_mach = j;
            best_gap_end = sol.curr_holes(j,sol.curr_tope(j))-sol.curr_holes(j,sol.curr_tope(j)-1);
        end
    end
    
    for i=1:size(WORKLOAD,1)
        % Tasks are sorted by arrival time
        BM = -1; % Best machine
        BH = -1; % Best hole in machine BM
        BFL = Inf; % Best finish time
        for j=1:MAX_NRO_SERVERS
            k = 1;
            while (WORKLOAD(i,3) > sol.curr_holes(j,k))
                % Deadline is taken into account for searching holes
                % Search in holes
                STH = sol.curr_holes(j,k); % Starting time of current hole
                ETH = sol.curr_holes(j,k+1); % Ending time of current hole
                starting_time = max(STH,WORKLOAD(i,1));
                if (ETH - starting_time >= WORKLOAD(i,2))
                    % Task i fits in hole k in machine j
                    % Deadline IS NOT CONSIDERED here -> include ??
                    if (ETH - starting_time - WORKLOAD(i,2) < BFL)
                        BFL = ETH - starting_time - WORKLOAD(i,2);
                        BM = j;
                        BH = k;
                    end
                end
                if (k < sol.curr_tope(j)-1)
                    k = k+2; % Next hole
                else
                    break
                end
            end % search hole in next machine
        end % End of hole search
        if (BM > -1) % Hole found
            % Insert in hole BH, modify hole BH, assign task to BM
            % Modify mak_local, compute best_end_gap_machine
            if (WORKLOAD(i,1) <= sol.curr_holes(BM,BH))
                % task arrived before hole starting time 
                % update starting time
                starting_time = sol.curr_holes(BM,BH);
                ending_time = sol.curr_holes(BM,BH)+WORKLOAD(i,2);
                
                sol.tasks_assign(i) = BM;
                sol.tasks_start(i) = starting_time;
                
                % update hole end
                sol.curr_holes(BM,BH) = sol.curr_holes(BM,BH)+WORKLOAD(i,2);                
            else
                % task arrived after hole starting time
                % insert new hole and shift the rest
                orig_end = sol.curr_holes(BM,BH+1);
                sol.curr_holes(BM,BH+1) = WORKLOAD(i,1);
                sol.curr_tope(BM) = sol.curr_tope(BM)+2;
                for h=sol.curr_tope(BM):-1:BH+4
                    % shift right two positions 
                    sol.curr_holes(BM,h)=sol.curr_holes(BM,h-2);
                end
                sol.curr_holes(BM,BH+2) = WORKLOAD(i,1)+WORKLOAD(i,2);
                sol.curr_holes(BM,BH+3) = orig_end;
                
                starting_time = WORKLOAD(i,1);
                ending_time = WORKLOAD(i,1) + WORKLOAD(i,2);
                
                sol.tasks_assign(i) = BM;
                sol.tasks_start(i) = starting_time;
            end

            %assign(i) = BM;
            
            assign(BM, assign_tope(BM)) = i;    
            assign_tope(BM) = assign_tope(BM) + 1;
            
            if (starting_time + WORKLOAD(i,2) > WORKLOAD(i,3))
                sol.n_violations = sol.n_violations + 1;
                sol.t_violations = sol.t_violations + (starting_time + WORKLOAD(i,2) - WORKLOAD(i,3));
            end

            %mak_local(BM) = ending_time;    
        else % no hole found, schedule at the end of best machine
            %assign(i) = best_gap_end_mach;
            
            sol.tasks_assign(i) = best_gap_end_mach;
            sol.tasks_start(i) = sol.curr_holes(best_gap_end_mach,sol.curr_tope(best_gap_end_mach)-1);
            
            assign(best_gap_end_mach,assign_tope(best_gap_end_mach)) = i;
            assign_tope(best_gap_end_mach) = assign_tope(best_gap_end_mach) + 1;
            
            if (sol.curr_holes(best_gap_end_mach,sol.curr_tope(best_gap_end_mach)-1) + WORKLOAD(i,2) > WORKLOAD(i,3));
                sol.n_violations = sol.n_violations + 1;
                sol.t_violations = sol.t_violations + (sol.curr_holes(best_gap_end_mach,sol.curr_tope(best_gap_end_mach)-1) + WORKLOAD(i,2) - WORKLOAD(i,3));
            end
            
            %mak_local(best_gap_end_mach) = sol.curr_holes(best_gap_end_mach,sol.curr_tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
            sol.curr_holes(best_gap_end_mach,sol.curr_tope(best_gap_end_mach)-1) = sol.curr_holes(best_gap_end_mach,sol.curr_tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
        end
        
        if sol.tasks_start(i)+WORKLOAD(i,2) > 151
            sol.makespan = Inf;
            return;
        else
            for t = sol.tasks_start(i):sol.tasks_start(i)+WORKLOAD(i,2)-1
                sol.taskPower(t) = sol.taskPower(t) + POWER_DIFF;
            end
        end
        
        % find best_end_gap_mach
        best_gap_end = -Inf;
        for j=1:MAX_NRO_SERVERS
            if (sol.curr_holes(j,sol.curr_tope(j))-sol.curr_holes(j,sol.curr_tope(j)-1) > best_gap_end)
                best_gap_end_mach = j;
                best_gap_end = sol.curr_holes(j,sol.curr_tope(j))-sol.curr_holes(j,sol.curr_tope(j)-1);
            end
        end
    end

    sol.makespan = 0;       
    for i = 1:size(WORKLOAD,1)
        if sol.tasks_start(i)+WORKLOAD(i,2) > sol.makespan
            sol.makespan = sol.tasks_start(i)+WORKLOAD(i,2);
        end
    end
end

