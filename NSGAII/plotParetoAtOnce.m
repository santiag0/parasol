function f = plotParetoAtOnce(pathFromSchedDIR)
%this function plots the pareto fron using plotPareto. 
%its arguments specifies the path to the PF file within the scheduler
%directory. For example, for : {BSD,BFH,EFT}/p3/log.50_10_5_100_1.L.p3.BSD.(75-500).FP.global, the
%argument is p3/log.50_10_5_100_1.L.p3.BSD.(75-500).FP.global

fileBSD = dir(['./analisis_experimental/results/BSD/' pathFromSchedDIR '/*.FP.global']);
fileBFH = dir(['./analisis_experimental/results/BFH/' pathFromSchedDIR '/*.FP.global']);
fileEFT = dir(['./analisis_experimental/results/EFT/' pathFromSchedDIR '/*.FP.global']);

figure();
plotPareto(['./analisis_experimental/results/BSD/' pathFromSchedDIR '/' fileBSD.name], 2);
hold on;
plotPareto(['./analisis_experimental/results/BFH/' pathFromSchedDIR '/' fileBFH.name], 1)
plotPareto(['./analisis_experimental/results/EFT/' pathFromSchedDIR '/' fileEFT.name], 3)

view(63.5,32);

legend('BFH', 'BSD', 'EFT');

end
