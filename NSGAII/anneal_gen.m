function current = anneal_gen(current)
    global MAX_ENERGY_SERVER;
    global IDLE_ENERGY_SERVER;
    global WORKLOAD;
    global MAX_NRO_SERVERS;

    POWER_DIFF = MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER;

    if current.ref_power_total > current.sol.obj_power_total
        current.ref_power_total = current.sol.obj_power_total;
    end
    
    if current.ref_budget > current.sol.obj_budget
        current.ref_budget = current.sol.obj_budget;
    end
    
    if current.ref_violations > current.sol.obj_violations
        current.ref_violations = current.sol.obj_violations;
    end
    
    if current.sol.makespan > 150
        display('!!!! Como lleg� un infactible?');
        current.sol.makespan = Inf;
        return;
    end
    
    cant_movs = 1; %randi(2,1);
   
    for i = 1:cant_movs
        task = randi(size(WORKLOAD,1),1);
        
        task_start = WORKLOAD(task,1);
        task_length = WORKLOAD(task,2);
        task_deadline = WORKLOAD(task,3);
        
        if task_length == 0
            return;
        end
        
        start_mach = randi(MAX_NRO_SERVERS,1);
        old_mach = current.sol.tasks_assign(task);
        
        rescheduled = 0;
        new_mach = start_mach;
        
        new_task_start = -1;
        new_task_mach = -1;
        
        while (rescheduled == 0)
            if (new_mach ~= old_mach)               
                hole_idx = randi(current.sol.curr_tope(new_mach), 1);
                if (mod(hole_idx,2) == 0)
                    hole_idx = hole_idx - 1;
                end
                hole_iter = 0;
                
                while (hole_iter <= current.sol.curr_tope(new_mach)) && (rescheduled == 0)
                    hole_iter = hole_iter + 2;
                    hole_idx = hole_idx + 2;
                    if (hole_idx > current.sol.curr_tope(new_mach))
                        hole_idx = 1;
                    end

                    hole_start = current.sol.curr_holes(new_mach,hole_idx);
                    hole_end = current.sol.curr_holes(new_mach,hole_idx+1);

                    if (task_start < hole_end) && (task_length <= min(hole_end-task_start,hole_end-hole_start))
                        for t = max(hole_start,task_start):max(hole_start,task_start)+task_length-1
                            current.sol.taskPower(t) = current.sol.taskPower(t) + POWER_DIFF;
                        end

                        if task_start <= hole_start
                            % Lower bound of the hole
                            current.sol.curr_holes(new_mach,hole_idx) = hole_start + task_length;

                            if current.sol.curr_holes(new_mach,hole_idx) > task_deadline
                                current.sol.n_violations = current.sol.n_violations + 1;
                                current.sol.t_violations = current.sol.t_violations + (current.sol.curr_holes(new_mach,hole_idx) - task_deadline + 1);
                            end
                            
                            new_task_start = hole_start;
                            new_task_mach = new_mach;
                        elseif task_start + task_length == hole_end
                            % Upper bound of the hole
                            current.sol.curr_holes(new_mach,hole_idx+1) = hole_end - task_length;

                            if hole_end >= task_deadline
                                current.sol.n_violations = current.sol.n_violations + 1;
                                current.sol.t_violations = current.sol.t_violations + (hole_end - task_deadline + 1);
                            end
                            
                            new_task_start = task_start;
                            new_task_mach = new_mach;
                        else                           
                            aux_holes = current.sol.curr_holes(new_mach,:);
                            current.sol.curr_holes(new_mach,hole_idx+1) = task_start;
                            current.sol.curr_holes(new_mach,hole_idx+2) = task_start+task_length;
                            current.sol.curr_holes(new_mach,hole_idx+3) = aux_holes(hole_idx+1);

                            for h = hole_idx+2:current.sol.curr_tope(new_mach)
                                current.sol.curr_holes(new_mach,h+2) = aux_holes(h);
                            end

                            current.sol.curr_tope(new_mach) = current.sol.curr_tope(new_mach)+2;

                            if task_start+task_length > task_deadline
                                current.sol.n_violations = current.sol.n_violations + 1;
                                current.sol.t_violations = current.sol.t_violations + (task_start+task_length - task_deadline + 1);
                            end
                            
                            new_task_start = task_start;
                            new_task_mach = new_mach;
                        end                        

                        rescheduled = 1;
                    end
                end  
            end
            
            if (rescheduled == 0)
                new_mach = mod(new_mach, MAX_NRO_SERVERS) + 1;
               
                if (start_mach == new_mach)
                    current.sol.makespan = Inf;
                    return;
                end                
            end
        end        
    end   
    
    if (rescheduled == 1)
        for t = current.sol.tasks_start(task):current.sol.tasks_start(task)+WORKLOAD(task,2)-1
            if t <= 150
                current.sol.taskPower(t) = current.sol.taskPower(t) - POWER_DIFF;
            else
                display('!!!!! Pucha!');
            end
        end

        if current.sol.tasks_start(task) + task_length > task_deadline
            current.sol.n_violations = current.sol.n_violations - 1;
            current.sol.t_violations = current.sol.t_violations - (current.sol.tasks_start(task) + task_length - task_deadline + 1);
        end
        
        reshaped = 0;
        hole_idx = 1;
        while hole_idx < current.sol.curr_tope(old_mach) && (reshaped == 0)
            if current.sol.curr_holes(old_mach, hole_idx) == current.sol.tasks_start(task) + task_length
                current.sol.curr_holes(old_mach, hole_idx) = current.sol.curr_holes(old_mach, hole_idx) - task_length;
                reshaped = 1;
            elseif current.sol.curr_holes(old_mach, hole_idx+1) == current.sol.tasks_start(task)
                current.sol.curr_holes(old_mach, hole_idx+1) = current.sol.curr_holes(old_mach, hole_idx+1) + task_length;
                reshaped = 1;
            end
            
            hole_idx = hole_idx + 2;
        end
        
        if reshaped == 0
            hole_idx = 1;   
            while (hole_idx < current.sol.curr_tope(old_mach)) && (reshaped == 0)
                if current.sol.curr_holes(old_mach, hole_idx) > current.sol.tasks_start(task)
                    current.sol.curr_holes(old_mach, hole_idx+2:current.sol.curr_tope(old_mach)+2) = current.sol.curr_holes(old_mach, hole_idx:current.sol.curr_tope(old_mach));
                    current.sol.curr_holes(old_mach, hole_idx) = current.sol.tasks_start(task);
                    current.sol.curr_holes(old_mach, hole_idx+1) = current.sol.tasks_start(task) + task_length;
                    current.sol.curr_tope(old_mach) = current.sol.curr_tope(old_mach) + 2;
                    
                    reshaped = 1;
                end
                
                hole_idx = hole_idx + 2;
            end
        end
        
        if reshaped == 0
            display('ERROR!!!!');
            current.sol.makespan = Inf;
            return
        end
        
        current.sol.tasks_start(task) = new_task_start;
        current.sol.tasks_assign(task) = new_task_mach;
        
        current.sol.makespan = 0;       
        for i = 1:size(WORKLOAD,1)
            if current.sol.tasks_start(i)+WORKLOAD(i,2) > current.sol.makespan
                current.sol.makespan = current.sol.tasks_start(i)+WORKLOAD(i,2);
            end
        end
    end
    
    [current.sol.obj_power_total,current.sol.obj_budget,current.sol.obj_violations] = anneal_objs(current);
end

