function [assign,makespan,n_violations,t_violations,server_power,off_period_tasks] = scheduler(WORKLOAD,Servers,V,coolingPower)
%SCHEDULER 
%   Detailed explanation goes here

global MAX_ENERGY_SERVER;
global IDLE_ENERGY_SERVER;
global SLEEP_ENERGY_SERVER;
global MAX_NRO_SERVERS;
global GEN_NUM;
global SA_GEN_START;
global SA_PROB;
global SA_VERSION;
global SA_ITERS;

% DEBUG
% MAX_ENERGY_SERVER = 30;
% IDLE_ENERGY_SERVER = 22;
% SLEEP_ENERGY_SERVER = 3;
% MAX_NRO_SERVERS = 4;
% WORKLOAD(:,2) = round(WORKLOAD(:,2)/3);
% WORKLOAD(:,3) = round(WORKLOAD(:,3)+rand(1)*20);
% END_DEBUG

server_status = floor(Servers/MAX_ENERGY_SERVER);

global SCHEDULER_ID; % Now it is set in nsga-2

sim_period = V/2;

makespan = 0;

MACH_HOLES = zeros(MAX_NRO_SERVERS,1);

tope = ones(MAX_NRO_SERVERS,1);
time_on = zeros(MAX_NRO_SERVERS,1);
time_off = zeros(MAX_NRO_SERVERS,1);

% Dealdline violations (totel time and number)
t_violations = 0;
n_violations = 0;

% Tasks finishing outside the simulation period
off_period_tasks = 0;

energy = zeros(sim_period,1);

t = 1;
% First timestep: init machines
for j=1:server_status(1)
    % ON machines
    time_on(j) = 1;
    time_off(j) = 0;
    MACH_HOLES(j,1) = 1;
    tope(j) = tope(j)+1;
    mak_local(j) = 0;
    energy(1) = energy(1) + IDLE_ENERGY_SERVER; 
end
for j=server_status(1)+1:MAX_NRO_SERVERS
    % OFF machines
    time_on(j) = Inf;
    time_off(j) = 1;
    mak_local(j) = 0;
    energy(1) = energy(1) + SLEEP_ENERGY_SERVER; 
end

%DEBUG
%time_on
%time_off

% Insert holes
for t=2:sim_period
    if (server_status(t) < server_status(t-1))
        for h=1:server_status(t-1)-server_status(t)
            % Turn OFF server
            % Select OFF machine: sort ascending time_on
            [b,ind] = sort(time_on);
            off_mach = ind(1);
            time_on(off_mach) = t;
            time_off(off_mach) = t;
            % Hole ended in time t
            MACH_HOLES(off_mach,tope(off_mach)) = t;
            %energy(t) = energy(t) + SLEEP_ENERGY_SERVER;
            tope(off_mach) = tope(off_mach)+1;
        end
    elseif (server_status(t) > server_status(t-1))
        for h=1:server_status(t)-server_status(t-1)
            % Turn ON server
            % Select ON machine: sort descending time_off
            [b,ind] = sort(time_off,'descend');
            on_mach = ind(1);
            time_on(on_mach) = t;
            time_off(on_mach) = 0;
            % Hole starts in time t
            MACH_HOLES(on_mach,tope(on_mach)) = t;
            %energy(t) = energy(t) + IDLE_ENERGY_SERVER;
            tope(on_mach) = tope(on_mach)+1;
        end
    end
    % Init energy: all IDLE
    energy(t) = IDLE_ENERGY_SERVER * MAX_NRO_SERVERS;
end

best_gap_end = -Inf;
for j=1:MAX_NRO_SERVERS
    if (mod(tope(j),2) == 0)
        MACH_HOLES(j,tope(j))=sim_period+1;
    else
        MACH_HOLES(j,tope(j))=sim_period+1;
        tope(j)=tope(j)+1;
        MACH_HOLES(j,tope(j))=sim_period+1;
    end
    if (MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1) > best_gap_end)
        best_gap_end_mach = j;
        best_gap_end = MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1);
    end
    if MACH_HOLES(j,1) > 1
        % Machine off since init time
        for s=2:MACH_HOLES(j,1)-1
            energy(s) = energy(s)-(IDLE_ENERGY_SERVER-SLEEP_ENERGY_SERVER);
        end
    end
    for k=2:2:tope(j)-1
        % Reduce energy for OFF servers
        for s=MACH_HOLES(j,k):MACH_HOLES(j,k+1)-1
            energy(s) = energy(s)-(IDLE_ENERGY_SERVER-SLEEP_ENERGY_SERVER);
        end
    end
end
% Holes ready: scheduling !!

%DEBUG
%MACH_HOLES

switch SCHEDULER_ID
    case 1 % BFH: Best Fit Hole
        makespan = 0;
        
        for i=1:size(WORKLOAD,1)
            % Tasks are sorted by arrival time
            BM = -1; % Best machine
            BH = -1; % Best hole in machine BM
            BFL = Inf; % Best fit length
            for j=1:MAX_NRO_SERVERS
                k = 1;
                while (WORKLOAD(i,3) > MACH_HOLES(j,k))
                    % Deadline is taken into account for searching holes
                    % Search in holes
                    STH = MACH_HOLES(j,k); % Starting time of current hole
                    ETH = MACH_HOLES(j,k+1); % Ending time of current hole
                    starting_time = max(STH,WORKLOAD(i,1));
                    if (ETH - starting_time >= WORKLOAD(i,2))
                        % Task i fits in hole k in machine j
                        % Deadline IS NOT CONSIDERED here -> include ??
                        if (ETH - starting_time - WORKLOAD(i,2) < BFL)
                            BFL = ETH - starting_time - WORKLOAD(i,2);
                            BM = j;
                            BH = k;
                        end
                    end
                    if (k < tope(j)-1)
                        k = k+2; % Next hole
                    else
                        break
                    end
                end % search hole in next machine
            end % End of hole search
            if (BM > -1) % Hole found
                % Insert in hole BH, modify hole BH, assign task to BM
                % Modify mak_local, compute best_end_gap_machine
                if (WORKLOAD(i,1) <= MACH_HOLES(BM,BH))
                    % task arrived before hole starting time 
                    % update starting time
                    starting_time = MACH_HOLES(BM,BH);
                    ending_time = MACH_HOLES(BM,BH)+WORKLOAD(i,2);
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                    
                    % update hole end
                    MACH_HOLES(BM,BH) = MACH_HOLES(BM,BH)+WORKLOAD(i,2);                
                else
                    % task arrived after hole starting time
                    % insert new hole and shift the rest
                    orig_end = MACH_HOLES(BM,BH+1);
                    MACH_HOLES(BM,BH+1) = WORKLOAD(i,1);
                    tope(BM) = tope(BM)+2;
                    for h=tope(BM):-1:BH+4
                        % shift right two positions 
                        MACH_HOLES(BM,h)=MACH_HOLES(BM,h-2);
                    end
                    MACH_HOLES(BM,BH+2) = WORKLOAD(i,1)+WORKLOAD(i,2);
                    MACH_HOLES(BM,BH+3) = orig_end;

                    starting_time = WORKLOAD(i,1);
                    ending_time = WORKLOAD(i,1) + WORKLOAD(i,2);
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                end
                assign(i) = BM;
                if (starting_time + WORKLOAD(i,2) > WORKLOAD(i,3))
                    % disp(sprintf('T%d (ST: %d, ET: %d, deadline: %d)', i, starting_time, WORKLOAD(i,2), WORKLOAD(i,3)));
                    n_violations = n_violations + 1;
                    t_violations = t_violations + (starting_time + WORKLOAD(i,2) - WORKLOAD(i,3));
                end
                for s=starting_time:starting_time + WORKLOAD(i,2)-1
                    energy(s) = energy(s) + (MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER);
                end
                %mak_local(BM) = ending_time;    
            else % no hole found, schedule at the end of best machine
                assign(i) = best_gap_end_mach;
                if (MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2) > WORKLOAD(i,3));
                    % disp(sprintf('T%d: (ST: %d, ET: %d, deadline: %d)', i, MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1), WORKLOAD(i,2), WORKLOAD(i,3)));
                    n_violations = n_violations + 1;
                    t_violations = t_violations + (MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2) - WORKLOAD(i,3));
                end
                % Add energy
                starting_time = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1);
                ending_time = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1)+WORKLOAD(i,2);
                               
                if ending_time <= sim_period
                    for s=starting_time:ending_time-1
                        energy(s) = energy(s) + (MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER);
                    end
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                else
                    makespan = Inf;
                end                
                
                %mak_local(best_gap_end_mach) = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
                MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
            end
            % find best_end_gap_mach
            best_gap_end = -Inf;
            for j=1:MAX_NRO_SERVERS
                if (MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1) > best_gap_end)
                    best_gap_end_mach = j;
                    best_gap_end = MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1);
                end
            end
            % DEBUG
            %i
            %assign(i)
            %starting_time
            %ending_time
            %MACH_HOLES
            %energy
        end
        
        %makespan = mak_local(1);
        %for j=2:MAX_NRO_SERVERS
        %    if (mak_local(j) > makespan)
        %        makespan = mak_local(j);
        %    end
        %end

        server_power = energy';
        
    case 2 % EFTH : Earliest Finishing Time Hole
        makespan = 0;
        
        for i=1:size(WORKLOAD,1)
            % Tasks are sorted by arrival time
            BM = -1; % Best machine
            BH = -1; % Best hole in machine BM
            BFT = Inf; % Best finish time
            for j=1:MAX_NRO_SERVERS
                k = 1;
                found = 0;
                while ((WORKLOAD(i,3) > MACH_HOLES(j,k)) & ~found)
                    % Deadline is taken into account for searching holes
                    % Search in holes
                    STH = MACH_HOLES(j,k); % Starting time of current hole
                    ETH = MACH_HOLES(j,k+1); % Ending time of current hole
                    starting_time = max(STH,WORKLOAD(i,1));
                    if (ETH - starting_time >= WORKLOAD(i,2))
                        % Task i fits in hole k in machine j
                        % Deadline IS NOT CONSIDERED here -> include ??
                        if (ETH < BFT)
                            BFT = ETH;
                            BM = j;
                            BH = k;
                            % VER: is it OK ?
                            found = 1;
                        end
                    end
                    if (k < tope(j)-1)
                        k = k+2; % Next hole
                    else
                        break
                    end
                end % search hole in next machine
            end % End of hole search
            if (BM > -1) % Hole found
                % Insert in hole BH, modify hole BH, assign task to BM
                % Modify mak_local, compute best_end_gap_machine
                if (WORKLOAD(i,1) <= MACH_HOLES(BM,BH))
                    % task arrived before hole starting time 
                    % update starting time
                    starting_time = MACH_HOLES(BM,BH);
                    ending_time = MACH_HOLES(BM,BH)+WORKLOAD(i,2);
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                    
                    % update hole end
                    MACH_HOLES(BM,BH) = MACH_HOLES(BM,BH)+WORKLOAD(i,2);                
                else
                    % task arrived after hole starting time
                    % insert new hole and shift the rest
                    orig_end = MACH_HOLES(BM,BH+1);
                    MACH_HOLES(BM,BH+1) = WORKLOAD(i,1);
                    tope(BM) = tope(BM)+2;
                    for h=tope(BM):-1:BH+4
                        % shift right two positions 
                        MACH_HOLES(BM,h)=MACH_HOLES(BM,h-2);
                    end
                    MACH_HOLES(BM,BH+2) = WORKLOAD(i,1)+WORKLOAD(i,2);
                    MACH_HOLES(BM,BH+3) = orig_end;
                    starting_time = WORKLOAD(i,1);
                    ending_time = WORKLOAD(i,1) + WORKLOAD(i,2);
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                end
                assign(i) = BM;
                if (starting_time + WORKLOAD(i,2) > WORKLOAD(i,3))
                    % disp(sprintf('(ST: %d, ET: %d, deadline: %d)', starting_time, WORKLOAD(i,2), WORKLOAD(i,3)));
                    n_violations = n_violations + 1;
                    t_violations = t_violations + (starting_time + WORKLOAD(i,2) - WORKLOAD(i,3));
                end
                for s=starting_time:starting_time + WORKLOAD(i,2)-1
                    energy(s) = energy(s) + (MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER);
                end
                %mak_local(BM) = ending_time;    
            else % no hole found, schedule at the end of best machine
                assign(i) = best_gap_end_mach;
                if (MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2) > WORKLOAD(i,3));
                    % disp(sprintf('(ST: %d, ET: %d, deadline: %d)', MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1), WORKLOAD(i,2), WORKLOAD(i,3)));
                    n_violations = n_violations + 1;
                    t_violations = t_violations + (MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2) - WORKLOAD(i,3));
                end
                % Add energy
                starting_time = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1);
                ending_time = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1)+WORKLOAD(i,2);

                if ending_time <= sim_period
                    for s=starting_time:ending_time-1
                        energy(s) = energy(s) + (MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER);
                    end
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                else
                    makespan = Inf;
                end
                
                %mak_local(best_gap_end_mach) = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
                MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
            end
            % find best_end_gap_mach
            best_gap_end = -Inf;
            for j=1:MAX_NRO_SERVERS
                if (MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1) > best_gap_end)
                    best_gap_end_mach = j;
                    best_gap_end = MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1);
                end
            end
            % DEBUG
            %i
            %assign(i)
            %starting_time
            %ending_time
            %MACH_HOLES
            %energy
        end
        
        %makespan = mak_local(1);
        %for j=2:MAX_NRO_SERVERS
        %    if (mak_local(j) > makespan)
        %        makespan = mak_local(j);
        %    end
        %end

        server_power = energy';
        
    case 3 % BFH-D: Best Fit Hole Deadline
        makespan = 0;
        
        for i=1:size(WORKLOAD,1)
            % Tasks are sorted by arrival time
            BM = -1; % Best machine
            BH = -1; % Best hole in machine BM
            BFL = Inf; % Best fit length
            for j=1:MAX_NRO_SERVERS
                k = 1;
                while (WORKLOAD(i,3) > MACH_HOLES(j,k))
                    % Deadline is taken into account for searching holes
                    % Search in holes
                    STH = MACH_HOLES(j,k); % Starting time of current hole
                    ETH = MACH_HOLES(j,k+1); % Ending time of current hole
                    starting_time = max(STH,WORKLOAD(i,1));
                    if (ETH - starting_time >= WORKLOAD(i,2)) & ( starting_time + WORKLOAD(i,2) < WORKLOAD(i,3) )
                        % Task i fits in hole k in machine j
                        % Deadline IS CONSIDERED here 
                        if (ETH - starting_time - WORKLOAD(i,2) < BFL)
                            BFL = ETH - starting_time - WORKLOAD(i,2);
                            BM = j;
                            BH = k;
                        end
                    end
                    if (k < tope(j)-1)
                        k = k+2; % Next hole
                    else
                        break
                    end
                end % search hole in next machine
            end % End of hole search
            if (BM > -1) % Hole found
                % Insert in hole BH, modify hole BH, assign task to BM
                % Modify mak_local, compute best_end_gap_machine
                if (WORKLOAD(i,1) <= MACH_HOLES(BM,BH))
                    % task arrived before hole starting time 
                    % update starting time
                    starting_time = MACH_HOLES(BM,BH);
                    ending_time = MACH_HOLES(BM,BH)+WORKLOAD(i,2);
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                    
                    % update hole end
                    MACH_HOLES(BM,BH) = MACH_HOLES(BM,BH)+WORKLOAD(i,2);                
                else
                    % task arrived after hole starting time
                    % insert new hole and shift the rest
                    orig_end = MACH_HOLES(BM,BH+1);
                    MACH_HOLES(BM,BH+1) = WORKLOAD(i,1);
                    tope(BM) = tope(BM)+2;
                    for h=tope(BM):-1:BH+4
                        % shift right two positions 
                        MACH_HOLES(BM,h)=MACH_HOLES(BM,h-2);
                    end
                    MACH_HOLES(BM,BH+2) = WORKLOAD(i,1)+WORKLOAD(i,2);
                    MACH_HOLES(BM,BH+3) = orig_end;
                    
                    starting_time = WORKLOAD(i,1);
                    ending_time = WORKLOAD(i,1) + WORKLOAD(i,2);
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                end
                
                assign(i) = BM;
                if (starting_time + WORKLOAD(i,2) > WORKLOAD(i,3))
                    % disp(sprintf('T%d (ST: %d, ET: %d, deadline: %d)', i, starting_time, WORKLOAD(i,2), WORKLOAD(i,3)));
                    n_violations = n_violations + 1;
                    t_violations = t_violations + (starting_time + WORKLOAD(i,2) - WORKLOAD(i,3));
                end
                for s=starting_time:starting_time + WORKLOAD(i,2)-1
                    energy(s) = energy(s) + (MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER);
                end
                %mak_local(BM) = ending_time;    
            else % no hole found, schedule at the end of best machine
                assign(i) = best_gap_end_mach;
                if (MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2) > WORKLOAD(i,3));
                    % disp(sprintf('T%d: (ST: %d, ET: %d, deadline: %d)', i, MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1), WORKLOAD(i,2), WORKLOAD(i,3)));
                    n_violations = n_violations + 1;
                    t_violations = t_violations + (MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2) - WORKLOAD(i,3));
                end
                % Add energy
                starting_time = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1);
                ending_time = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1)+WORKLOAD(i,2);
                                
                if ending_time <= sim_period
                    for s=starting_time:ending_time-1
                        energy(s) = energy(s) + (MAX_ENERGY_SERVER - IDLE_ENERGY_SERVER);
                    end
                    
                    if ending_time > makespan
                        makespan = ending_time;
                    end
                else
                    makespan = Inf;
                end
                
                %mak_local(best_gap_end_mach) = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
                MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) = MACH_HOLES(best_gap_end_mach,tope(best_gap_end_mach)-1) + WORKLOAD(i,2);
            end
            % find best_end_gap_mach
            best_gap_end = -Inf;
            for j=1:MAX_NRO_SERVERS
                if (MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1) > best_gap_end)
                    best_gap_end_mach = j;
                    best_gap_end = MACH_HOLES(j,tope(j))-MACH_HOLES(j,tope(j)-1);
                end
            end
            % DEBUG
            %i
            %assign(i)
            %starting_time
            %ending_time
            %MACH_HOLES
            %energy
        end
        
        %makespan = mak_local(1);
        %for j=2:MAX_NRO_SERVERS
        %    if (mak_local(j) > makespan)
        %        makespan = mak_local(j);
        %    end
        %end

        server_power = energy';
    
    case 4 % SIMULATED ANNEALING
        %display(['> ==== STARTING SA ===== ', datestr(now)]);
        
        %init_sol = 1:1:(size(WORKLOAD,1)+MAX_NRO_SERVERS);
        %init_sol = randperm(size(WORKLOAD,1)+MAX_NRO_SERVERS-1);
        %init_sol = anneal_init_simple(tope, MACH_HOLES);
        
        %if randi(3,1,1) == 1
        %init_sol = anneal_init_efth(tope, MACH_HOLES, best_gap_end, best_gap_end_mach);
        %else
        init_sol = anneal_init_bd(tope, MACH_HOLES, best_gap_end, best_gap_end_mach);
        %end
        
        %init_sol = anneal_init_bfh(tope, MACH_HOLES, best_gap_end, best_gap_end_mach);
               
        if init_sol.makespan <= 150        
            current.sol = init_sol;
            current.tope = tope;
            current.holes = MACH_HOLES;
            current.V = V;
            current.basePower = energy;
            current.coolingPower = coolingPower;
            %current.eval = 1; % aleatorizar!!!
        
            if GEN_NUM < SA_GEN_START
                prob_sa = 2; 
            else
                if GEN_NUM == SA_GEN_START
                    display('STARTING SA EXECUTION...');
                end
                prob_sa = randi(SA_PROB,1,1);
            end
            
            %current = anneal_metrics(current);
                    
            if prob_sa == 1
                for iter = 1:SA_ITERS
                    %display('.');
                    
                    %current.eval = randi(iter,1,1);
                    current.eval = randi(3,1,1);
                    %current.eval = randi(5,1,1);
                    %current.eval = randi(7,1,1);

                    opt = anneal();
                    opt.Generator = @anneal_gen;
                    %opt.InitTemp = 1000;
                    %opt.InitTemp = 0.25; %0.50;
                    %opt.MaxConsRej = 10000; %50; %500;
                    %opt.MaxTries = 25; %100;
                    %opt.MaxSuccess = 10;
                    %opt.CoolSched = @(T) (.6*T);
                    opt.Verbosity = 0;

                    [current.ref_power_total,current.ref_budget,current.ref_violations] = anneal_objs(current);
                    [current.sol.obj_power_total,current.sol.obj_budget,current.sol.obj_violations] = anneal_objs(current);

                    if SA_VERSION == 0
                        [minimum,fval] = anneal(@anneal_eval,current,opt);
                    %elseif SA_VERSION == 1
                    else
                        [minimum,fval] = anneal_mo(@anneal_eval_mo,current,opt);
                    end

                    %if (current.sol.obj_power_total-minimum.sol.obj_power_total > 0) ...
                    %    || (current.sol.obj_budget-minimum.sol.obj_budget > 0) ...
                    %    || (current.sol.obj_violations-minimum.sol.obj_violations > 0)
                    
                    if (current.sol.obj_power_total-minimum.sol.obj_power_total >= 0) ...
                        && (current.sol.obj_budget-minimum.sol.obj_budget >= 0) ...
                        && (current.sol.obj_violations-minimum.sol.obj_violations >= 0)
                    
                        current = minimum;                
                        %display('GOLAZO!');
                    end
                end
            end

            if current.sol.makespan <= 150
                makespan = current.sol.makespan;
                n_violations = current.sol.n_violations;
                t_violations = current.sol.t_violations;
                server_power = (energy + current.sol.taskPower)';
            else
                makespan = Inf;
                server_power = energy;
                n_violations = Inf;
                t_violations = Inf;
            end
        else
            makespan = Inf;
            server_power = energy;
            n_violations = Inf;
            t_violations = Inf;
        end
        
        assign = [];
        off_sim = 0;
        
        %display(['> ==== ENDING SA ===== ', datestr(now)]);
end

% DEBUG ENERGY
% server_power = Servers;
% END_DEBUG ENERGY

% DEBUG
% Servers
% server_power
% server_power-Servers
% n_violations
% disp(sprintf('(%d,%d)',n_violations,t_violations))
% END_DEBUG

end

