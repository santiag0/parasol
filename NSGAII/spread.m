function s = spread(sols,ef1,ef2,ef3)
% spread metric for multiobjective optimization
% 3 objectives case: extreme points are ef1, ef2, ef3 (3D vectors)

np = numel(sols(:,1));
for i=1:np
    % find closest point to sols(i)
    best_dist(i) = Inf;
    for h=1:np
        if (h ~= i)
            % sols(i)
            % sols(h)
            d3D = dist_3D(sols(i,:),sols(h,:));
            if d3D < best_dist(i)
                best_dist(i) = d3D;
            end
        end
    end
end

% Find best dist to ef1
best_d_ef1 = Inf;
for i=1:np
    d3D = dist_3D(sols(i,:),ef1);
    if d3D < best_d_ef1
        best_d_ef1 = d3D;
    end
end

% Find best dist to ef2
best_d_ef2 = Inf;
for i=1:np
    d3D = dist_3D(sols(i,:),ef2);
    if d3D < best_d_ef2
        best_d_ef2 = d3D;
    end
end

% Find best dist to ef3
best_d_ef3 = Inf;
for i=1:np
    d3D = dist_3D(sols(i,:),ef3);
    if d3D < best_d_ef3
        best_d_ef3 = d3D;
    end
end

sum_dist = 0;
for i=1:np
    sum_dist = sum_dist + best_dist(i);
end
d_prom = sum_dist/(np-1);

sum = 0;
for i=1:np
    sum = sum + abs(d_prom - best_dist(i));
end

sum_ext = best_d_ef1 + best_d_ef2 + best_d_ef3;
s = (sum_ext + sum)/(sum_ext + np*d_prom);
