function fitness = anneal_eval(current)
    if current.sol.makespan > 150
        fitness = Inf;
    else
        if current.eval == 3 || current.eval == 5
            fitness = current.sol.obj_power_total;
        elseif current.eval == 2
            fitness = current.sol.obj_violations;
        elseif current.eval == 1 || current.eval == 4 || current.eval == 6
            fitness = current.sol.obj_budget;   
        else
            display('=== ERROR!!! ===');
        end
    end
end

