% non-deferrable workloads
% D = rdir('**\*log*L.p*FP');
% D = rdir('**\*log*L.p*FP.global');
% deferrable workloads
 D = rdir('**\*log*-def*FP');
% D = rdir('**\*log*-def*FP.global');
for ii=1:length(D)
    % disp(D(ii).name);
    fp = load(D(ii).name,'-ascii');
    % Best solutions for each objective
    bf = Inf*ones(3);
    maxf = zeros(3);
    np = numel(fp(:,1));
    nf = strsplit(D(ii).name,'log.');
    inst_name = strsplit(nf{2},'.p');
    str_log = sprintf('%s ',inst_name{1});
    for j=1:3
        % j: objective function index
        bsolf(j) = -1;
        for k=1:np
            % k: FP solution index
            if fp(k,j) < bf(j)
                bf(j) = fp(k,j);
                bsolf(j) = k;
            end
            if fp(k,j) > maxf(j)
                maxf(j) = fp(k,j);
            end
        end
        if bsolf == 1
            disp(sprintf('file %s: bsol is -1 for objective %d',D(ii).name,j));
        else
            str_log = sprintf('%s %.2f %.2f %.2f 0',str_log,fp(bsolf(j),1),fp(bsolf(j),2),fp(bsolf(j),3));
        end
    end
    % Nearest solution to ideal vector
    bdist = Inf;
    nsi = -1;
    for k=1:np
        if sqrt((fp(k,1)^2/maxf(1)^2)+(fp(k,2)^2/maxf(2)^2)+(fp(k,3)^2/maxf(3)^2)) < bdist
            bdist = sqrt((fp(k,1)^2/maxf(1)^2)+(fp(k,2)^2/maxf(2)^2)+(fp(k,3)^2/maxf(3)^2));
            nsi = k;
        end
    end
    str_log = sprintf('%s %.2f %.2f %.2f 0',str_log,fp(nsi,1),fp(nsi,2),fp(nsi,3));
    disp(str_log);
end;