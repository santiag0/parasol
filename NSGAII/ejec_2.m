global SA_GEN_START;
global SA_PROB;
global SA_VERSION;

warning('off','all');

% Semilla para la generaci�n de n�meros
rng(2048);
%rng('shuffle');

%x = dir('scenarios/workloads_red/workload_hi*');
x = dir('scenarios/workloads_red/workload_med*');

no_files = size(x);
for j=1:no_files(1,1)
    disp(x(j).name)
     
    fn = sprintf('scenarios/workloads_red/%s',x(j).name);
    disp(fn)
     
    for i=1:1
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p4.txt',1,i,'scenarios/powerProfiles/g1.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p3.txt',1,i,'scenarios/powerProfiles/g1.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p2.txt',1,i,'scenarios/powerProfiles/g1.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);        
    end
     
    for i=1:3
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p4.txt',1,i,'scenarios/powerProfiles/g2.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p3.txt',1,i,'scenarios/powerProfiles/g2.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);         
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p2.txt',1,i,'scenarios/powerProfiles/g2.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);
    end
    
    for i=1:3
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p4.txt',1,i,'scenarios/powerProfiles/g3.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p3.txt',1,i,'scenarios/powerProfiles/g3.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);         
        display(['> Inicio BFH: ', datestr(now)]);
        nsga_2(50,500,fn,'scenarios/powerProfiles/p2.txt',1,i,'scenarios/powerProfiles/g3.mat'); % BFH
        display(['Fin BFH: ', datestr(now)]);
    end
     
     for i=1:0
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',2,i,'scenarios/powerProfiles/g1.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',2,i,'scenarios/powerProfiles/g1.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',2,i,'scenarios/powerProfiles/g1.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',2,i,'scenarios/powerProfiles/g2.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',2,i,'scenarios/powerProfiles/g2.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',2,i,'scenarios/powerProfiles/g2.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',2,i,'scenarios/powerProfiles/g3.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',2,i,'scenarios/powerProfiles/g3.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
         display(['> Inicio EFTH: ', datestr(now)]);
         nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',2,i,'scenarios/powerProfiles/g3.mat'); % EFTH
         display(['Fin EFTH: ', datestr(now)]);
     end
     
     for i=1:0
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',3,i,'scenarios/powerProfiles/g1.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',3,i,'scenarios/powerProfiles/g1.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',3,i,'scenarios/powerProfiles/g1.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
%          
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',3,i,'scenarios/powerProfiles/g2.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',3,i,'scenarios/powerProfiles/g2.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
          display(['> Inicio BFH-D: ', datestr(now)]);
          nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',3,i,'scenarios/powerProfiles/g2.mat'); % BFH-D
          display(['Fin BFH-D: ', datestr(now)]);
%        
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',3,i,'scenarios/powerProfiles/g3.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',3,i,'scenarios/powerProfiles/g3.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
%          display(['> Inicio BFH-D: ', datestr(now)]);
%          nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',3,i,'scenarios/powerProfiles/g3.mat'); % BFH-D
%          display(['Fin BFH-D: ', datestr(now)]);
     end
     
     for i=1:0
        SA_GEN_START = -1;
        SA_PROB = 10;
        SA_VERSION=0;

        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',4,i,'scenarios/powerProfiles/g2.mat'); % SA
        display(['Fin SA: ', datestr(now)]);
        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',4,i,'scenarios/powerProfiles/g2.mat'); % SA
        display(['Fin SA: ', datestr(now)]);
        
        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',4,i,'scenarios/powerProfiles/g1.mat'); % SA
        display(['Fin SA: ', datestr(now)]);
        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',4,i,'scenarios/powerProfiles/g1.mat'); % SA
        display(['Fin SA: ', datestr(now)]);

        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p4.txt',4,i,'scenarios/powerProfiles/g3.mat'); % SA
        display(['Fin SA: ', datestr(now)]);       
        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p2.txt',4,i,'scenarios/powerProfiles/g3.mat'); % SA
        display(['Fin SA: ', datestr(now)]);

        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',4,i,'scenarios/powerProfiles/g1.mat'); % SA
        display(['Fin SA: ', datestr(now)]);
        
        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',4,i,'scenarios/powerProfiles/g3.mat'); % SA
        display(['Fin SA: ', datestr(now)]);
        
        display(['> Inicio SA: ', datestr(now)]);
        nsga_2(20,1000,fn,'scenarios/powerProfiles/p3.txt',4,i,'scenarios/powerProfiles/g2.mat'); % SA
        display(['Fin SA: ', datestr(now)]);     
     
     end
end

% x = dir('scenarios/workloads_red/workload_low*');
% no_files = size(x);
% for j=1:no_files(1,1)
%     disp(x(j).name)
%      
%     fn = sprintf('scenarios/workloads_red/%s',x(j).name);
%     disp(fn)
%      
%     for i=1:3
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p4.txt',1,i,'scenarios/powerProfiles/g1.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p3.txt',1,i,'scenarios/powerProfiles/g1.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p2.txt',1,i,'scenarios/powerProfiles/g1.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);        
%     end
%      
%     for i=1:3
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p4.txt',1,i,'scenarios/powerProfiles/g2.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p3.txt',1,i,'scenarios/powerProfiles/g2.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);         
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p2.txt',1,i,'scenarios/powerProfiles/g2.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);
%     end
%     
%     for i=1:3
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p4.txt',1,i,'scenarios/powerProfiles/g3.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p3.txt',1,i,'scenarios/powerProfiles/g3.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);         
%         display(['> Inicio BFH: ', datestr(now)]);
%         nsga_2(75,500,fn,'scenarios/powerProfiles/p2.txt',1,i,'scenarios/powerProfiles/g3.mat'); % BFH
%         display(['Fin BFH: ', datestr(now)]);
%     end
% end