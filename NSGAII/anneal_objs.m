function [power_total,budget,violations] = anneal_objs(current)
    global TOTAL_POWER_REF;
    global GREEN_FORECAST;
    global BROWN_ENERGY_BUY;
    global GREEN_ENERGY_SELL;

    basePower = current.basePower;
    coolingPower = current.coolingPower;
    violations = current.sol.t_violations;
    taskPower = current.sol.taskPower;
    
    power_dev = (coolingPower' + taskPower + basePower) - TOTAL_POWER_REF;
    power_dev(power_dev < 0) = 0;
    power_total = sum(power_dev);

    power = coolingPower + taskPower' + basePower';

    brown_power = power - GREEN_FORECAST';
    power_selled = brown_power;
    power_selled(power_selled > 0) = 0;

    power_bought = brown_power;
    power_bought(brown_power < 0) = 0;

    %budget = (power_bought * BROWN_ENERGY_BUY) + (power_selled * GREEN_ENERGY_SELL);
    budget = (power_bought * BROWN_ENERGY_BUY); % + (power_selled * GREEN_ENERGY_SELL);
end

