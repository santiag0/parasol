function printPareto2(f1,f2,f3,gen,scheduler_id)

ip = true(size(f1));

for k=1:numel(f1)
    for h=1:numel(f1)
        if (f1(h)<f1(k) & f2(h)<f2(k) & f3(h)<f3(k))
            ip(k) = false;
        end
    end
    if (f2(k) > 50) | (f3(k) > 500)
            ip(k) = false;
    end

%DEBUG
%ip
%figure
end
hold on
xlabel('f(1) - energy profile');
ylabel('f(2) - temperature profile');
zlabel('f(3) - QoS');
grid on
axis square

if (gen == 0) % Final Pareto
    if scheduler_id == 1
        plot3(f1(ip),f2(ip),f3(ip),'g+')
    elseif scheduler_id == 2
        plot3(f1(ip),f2(ip),f3(ip),'r+')
    else
        plot3(f1(ip),f2(ip),f3(ip),'b+')
    end
elseif (gen == 1) % Intital Pareto
    plot3(f1(ip),f2(ip),f3(ip),'y+')
else % Intermediate Pareto
    plot3(f1(ip),f2(ip),f3(ip),'m+')
    
end
f = gcf();
dcm_obj = datacursormode(f);
set(dcm_obj,'UpdateFcn',@showDatatips)

end