% Script para generar los Pareto fronts para cada instancia,
% a partir de los archivos de soluciones no dominadas encontradas en las 
% n ejecuciones independientes realizadas (archivos *.FP)
% utiliza finPareto con tal fin
%x = dir('scenarios/workloads/w*');
x = dir('scenarios/workloads/w*');
no_files = size(x);
pow = ['p4';'p3';'p2'];
%sch = ['BFH';'EFT';'BSD'];
sch = ['BSD'];
for j=1:no_files(1,1)
    disp(x(j).name)
    fw = strsplit(x(j).name,'workload_');
    name_inst = strsplit(fw{2},'.log');
    for s=1:1
        scheduler_name = sch(s,:);
        for p=1:3
            pow_prof = pow(p,:)
            dir_sols = sprintf('analisis_experimental/results/%s/%s/%s/',scheduler_name,pow_prof,name_inst{1})
            oldDir = pwd
            cd(dir_sols)
            file_sols = sprintf('log.%s.%s.%s.(75-500).FP',name_inst{1},pow_prof,scheduler_name);
            findPareto(file_sols);
            cd(oldDir);
        end
    end
end     