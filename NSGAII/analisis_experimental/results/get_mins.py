#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  get_mins.py
#  
#  Copyright 2015 Santiago Iturriaga - INCO <siturria@saxo.fing.edu.uy>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys

def main():
    #ALG_LIST = ["BFH","BSD","EFT", "SA"]
    ALG_LIST = ["EFT"]
    GREEN_SCEN_LIST = ["g1.mat", "g2.mat", "g3.mat"]
    NRG_SCEN_LIST = ["p2.txt", "p3.txt", "p4.txt"]
    LOAD_SCEN_LIST = ["hi","med"]

    for LOAD_SCEN in LOAD_SCEN_LIST:
        for GREEN_SCEN in GREEN_SCEN_LIST:
            for NRG_SCEN in NRG_SCEN_LIST:
                for ALG in ALG_LIST:
                    pf_path = "{0}/{1}/{2}/{3}/log.{3}.{2}.{0}.(20-500).FP".format(ALG,GREEN_SCEN,NRG_SCEN,LOAD_SCEN)

                    min_obj1 = float("inf")
                    min_obj2 = float("inf")
                    min_obj3 = float("inf")

                    with open(pf_path) as pf:
                        for line in pf:
                            obj_list = line.strip().split('  ')
                            obj1 = float(obj_list[0])
                            obj2 = float(obj_list[1])
                            obj3 = float(obj_list[2])

                            if min_obj1 > obj1:
                                min_obj1 = obj1
                                
                            if min_obj2 > obj2:
                                min_obj2 = obj2
                                
                            if min_obj3 > obj3:
                                min_obj3 = obj3

                    print("{0} {1} {2} {3} {4} {5} {6}".format(GREEN_SCEN, NRG_SCEN, LOAD_SCEN, ALG, min_obj1, min_obj2, min_obj3))
    
    return 0

if __name__ == '__main__':
    main()

