function s = spacing(sols)
% spacing metric for multiobjective optimization
% 3 objectives case

np = numel(sols(:,1))
for i=1:np
    % find closest point to sols(i)
    best_dist(i) = Inf;
    for h=1:np
        if (h ~= i)
            % sols(i)
            % sols(h)
            d3D = dist_3D(sols(i,:),sols(h,:));
            if d3D < best_dist(i)
                best_dist(i) = d3D;
            end
        end
    end
end

sum_dist = 0;
for i=1:np
    sum_dist = sum_dist + best_dist(i);
end
d_prom = sum_dist/np

sum = 0;
for i=1:np
    sum = sum + (d_prom - best_dist(i))^2;
end

s = sqrt(sum/(np-1));