function d = GD(sols,sols_pareto)

% generational distance metric for multiobjective optimization
% 3 objectives case
% inputs: approximated Pareto front, true Pareto front

d = 0;
np = numel(sols(:,1));
npf = numel(sols_pareto(:,1));
for i=1:np
    % find closest point to sols(i)
    best_dist = Inf;
    for h=1:npf
        d3D = dist_3D(sols(i,:),sols_pareto(h,:));
        if d3D < best_dist
            best_dist = d3D;
        end
    end
    d = d + best_dist;
end

