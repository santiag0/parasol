%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPAP112
% Project Title:
%    Portfolio Optimization using Classic and Intelligent Methods in MATLAB
% 
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function [z, out]=PortMOC(x,model)

    R=model.R;
    
    if isfield(model,'method')
        method=model.method;
    else
        method='';
    end
    
    if isfield(model,'alpha')
        alpha=model.alpha;
    else
        alpha='';
    end
    
    w=x/sum(x);

    [rsk, ret]=CalculatePortfolioObjectives(w, R, method, alpha);
    
    z=[rsk
       -ret];
    
    out.w=w;
    out.rsk=rsk;
    out.ret=ret;
    
end