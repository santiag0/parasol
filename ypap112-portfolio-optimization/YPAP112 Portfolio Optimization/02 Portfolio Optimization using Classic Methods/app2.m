%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPAP112
% Project Title:
%    Portfolio Optimization using Classic and Intelligent Methods in MATLAB
% 
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

clc;
clear;
close all;

data=load('mydata');

Symbols=data.Symbols;
R=data.R;

% Use Semi-Variance
Semi=0;

port=Portfolio();
[MU, SIGMA]=EstimateReturnMoments(R,Semi);
port=port.setAssetMoments(MU,SIGMA);
port=port.setDefaultConstraints();

W=port.estimateFrontier(100);
WReturn=port.estimatePortReturn(W);
WRisk=port.estimatePortRisk(W);

figure;
plot(WRisk,WReturn,'LineWidth',2);
hold on;
for i = 1:numel(Symbols)
    s = Symbols{i};
    mu = MU(i);
    sigma = sqrt(SIGMA(i,i));
    plot(sigma,mu,'ro','MarkerFaceColor','r');
    text(sigma+0.001,mu,s);
end
grid on;
xlabel('Risk (Variance)');
ylabel('Return (Mean)');
