%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPAP112
% Project Title:
%    Portfolio Optimization using Classic and Intelligent Methods in MATLAB
% 
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

clc;
clear;
close all;

cn=yahoo;

Symbols={'IBM','GOOGL','MSFT','AAPL','YHOO','AMZN'};

StartDate='2014-Jan-01';
EndDate='2014-Feb-01';

P=[];
R=[];

for Symbol = Symbols
    s = Symbol{1};
    data = cn.fetch(s,'Close',StartDate,EndDate);
    Date=data(:,1);
    p=data(:,2);
    r=price2ret(p);
    P=[P p]; %#ok
    R=[R r]; %#ok
end

cn.close();

save('mydata','Symbols','Date','P','R');
