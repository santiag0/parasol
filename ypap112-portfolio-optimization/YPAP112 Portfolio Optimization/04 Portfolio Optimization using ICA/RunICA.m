%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPAP112
% Project Title:
%    Portfolio Optimization using Classic and Intelligent Methods in MATLAB
% 
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function out = RunICA(model)

    %% Problem Definition

    CostFunction=@(x) PortCost(x,model);        % Cost Function

    nVar=size(model.R,2);  % Number of Decision Variables

    VarSize=[1 nVar];   % Decision Variables Matrix Size

    VarMin=0;         % Lower Bound of Variables
    VarMax=1;         % Upper Bound of Variables


    %% ICA Parameters

    MaxIt=150;          % Maximum Number of Iterations

    nPop=40;            % Population Size
    nEmp=5;             % Number of Empires/Imperialists

    alpha=1;            % Selection Pressure

    beta=0.9;           % Assimilation Coefficient

    pRevolution=0.1;    % Revolution Probability
    mu=0.05;            % Revolution Rate

    zeta=0.1;           % Colonies Mean Cost Coefficient

    global ProblemSettings;
    ProblemSettings.CostFunction=CostFunction;
    ProblemSettings.nVar=nVar;
    ProblemSettings.VarSize=VarSize;
    ProblemSettings.VarMin=VarMin;
    ProblemSettings.VarMax=VarMax;

    global ICASettings;
    ICASettings.MaxIt=MaxIt;
    ICASettings.nPop=nPop;
    ICASettings.nEmp=nEmp;
    ICASettings.alpha=alpha;
    ICASettings.beta=beta;
    ICASettings.pRevolution=pRevolution;
    ICASettings.mu=mu;
    ICASettings.zeta=zeta;

    %% Initialization

    % Initialize Empires
    emp=CreateInitialEmpires();

    % Array to Hold Best Cost Values
    BestCost=zeros(MaxIt,1);


    %% ICA Main Loop

    for it=1:MaxIt

        % Assimilation
        emp=AssimilateColonies(emp);

        % Revolution
        emp=DoRevolution(emp);

        % Intra-Empire Competition
        emp=IntraEmpireCompetition(emp);

        % Update Total Cost of Empires
        emp=UpdateTotalCost(emp);

        % Inter-Empire Competition
        emp=InterEmpireCompetition(emp);

        % Update Best Solution Ever Found
        imp=[emp.Imp];
        [~, BestImpIndex]=min([imp.Cost]);
        BestSol=imp(BestImpIndex);

        % Update Best Cost
        BestCost(it)=BestSol.Cost;

        % Show Iteration Information
        disp(['Iteration ' num2str(it) ': Best Cost = ' num2str(BestCost(it))]);

    end

    %% Results

    out.BestSol=BestSol;
    out.BestCost=BestCost;

end