%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPAP112
% Project Title:
%    Portfolio Optimization using Classic and Intelligent Methods in MATLAB
% 
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function [z, out]=PortCost(x,model)

    DesiredRet=model.DesiredRet;

    [~, out]=PortMOC(x,model);
    
    rsk = out.rsk;
    ret = out.ret;
    
    viol=max(0,1-ret/DesiredRet);
    beta=1000;
    
    z=rsk*(1+beta*viol);
    
    out.viol=viol;
    out.IsFeasible=(viol==0);
    
end