%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPAP112
% Project Title:
%    Portfolio Optimization using Classic and Intelligent Methods in MATLAB
% 
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

clc;
clear;
close all;

%% Run PSO

data=load('mydata');
R=data.R;

nAsset=size(R,2);
MinRet=min(mean(R,1));
MaxRet=max(mean(R,1));

nSol=15;
DR=linspace(MinRet,MaxRet,nSol);

model.R=R;
model.method='cvar';
model.alpha=0.95;

W=zeros(nSol,nAsset);
WReturn=zeros(nSol,1);
WRisk=zeros(nSol,1);

for k=1:nSol
    model.DesiredRet=DR(k);
    disp(['Running for Solution #' num2str(k) ':']);
    out = RunPSO(model);
    disp('__________________________');
    disp('');
    W(k,:)=out.BestSol.Out.w;
    WReturn(k)=out.BestSol.Out.ret;
    WRisk(k)=out.BestSol.Out.rsk;
end

EF=find(~IsDominated(WRisk,WReturn));

%% Results
figure;
plot(WRisk,WReturn);
hold on;
plot(WRisk(EF),WReturn(EF),'r','LineWidth',2);
grid on;
xlabel('Risk');
ylabel('Return');
