%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPAP112
% Project Title:
%    Portfolio Optimization using Classic and Intelligent Methods in MATLAB
% 
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%

function [rsk, ret]=CalculatePortfolioObjectives(w, R, method, alpha)

    w=reshape(w,[],1);

    % Methods
    % mv = Mean-Variance
    % msv = Mean-Semi-Variance
    % mad = Mean-Absolute-Deviation
    % cvar = Conditional Value at Risk

    if ~exist('method','var') || isempty(method)
        method = 'mv';
    end
    
    if ~exist('alpha','var') || isempty(alpha)
        alpha = 0.95;
    end

    method=lower(method);
    
    switch method
        case 'mad'
            port=PortfolioMAD();
            port=port.setScenarios(R);
            
        case 'cvar'
            port=PortfolioCVaR();
            port=port.setScenarios(R);
            port=port.setProbabilityLevel(alpha);
            
        otherwise
            Semi=strcmp(method,'msv');
            [MU, SIGMA]=EstimateReturnMoments(R,Semi);
            port=Portfolio();
            port=port.setAssetMoments(MU,SIGMA);
    end
    
    port=port.setDefaultConstraints();
    rsk = port.estimatePortRisk(w);
    ret = port.estimatePortReturn(w);
    
end