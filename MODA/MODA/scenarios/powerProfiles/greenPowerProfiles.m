% Semilla para la generaci�n de n�meros
rng(1);

% ============ ESCENARIO: 10:00 a 12:30 =================

% Max. generaci�n 1.0kW
% 08:00-10:00 0.1kW
% 10:00-11:00 0.2kW
% 11:00-12:00 0.8kW
% 12:00-14:00 1.0kW
% 14:00-15:00 0.8kW
% 15:00-16:00 0.2kW
% 16:00-17:00 0.1kW
%GREEN_GENERATION = vertcat(rand(25,1) .* 28 + ones(25,1) * 172, ...
%    rand(100,1) .* 112 + ones(100,1) * 688, ...
%    rand(25,1) .* 140 + ones(25,1) * 860);

GREEN_GENERATION = vertcat(ones(25,1) * 160, ...
    linspace(160, 640, 25)', ...
    ones(50,1) * 640, ...
    linspace(640, 800, 25)', ...
    ones(25,1) * 800);

GREEN_GENERATION = vertcat(rand(25,1) .* 40 + GREEN_GENERATION(1:25), ...
    rand(25,1) .* 160 + GREEN_GENERATION(26:50), ...
    rand(50,1) .* 160 + GREEN_GENERATION(51:100), ...
    rand(25,1) .* 200 + GREEN_GENERATION(101:125), ...
    rand(25,1) .* 200 + GREEN_GENERATION(126:150));

% Se considera un 14% de error en la predicci�n
GREEN_FORECAST_ERROR = vertcat(ones(150,1) * ((rand(1,1).*0.14)-(ones(1,1).*0.07)));
GREEN_FORECAST = vertcat(ones(35,1) * mean(GREEN_GENERATION(1:35)), ...
    ones(80,1) * mean(GREEN_GENERATION(36:115)), ...
    ones(35,1) * mean(GREEN_GENERATION(116:150)));
GREEN_FORECAST = GREEN_FORECAST + GREEN_FORECAST .* GREEN_FORECAST_ERROR;

GREEN_GENERATION = GREEN_GENERATION ./ 2;
GREEN_FORECAST = GREEN_FORECAST ./ 2;

%BROWN_ENERGY_BUY = ones(150,1) * 2.885;
BROWN_ENERGY_BUY = vertcat(ones(75,1) * 1.380, ones(75,1) * 2.885);
% Precios MC2: valle=1.380 llano=2.885 punta=5.600
% Punta: de 18:00 a 22:00 hrs.
% Llano: de 07:00 a 18:00 y de 22:00 a 24:00 hrs.
% Valle: de 00:00 a 07:00 hrs.

GREEN_GENERATION_G1 = GREEN_GENERATION

save g1.mat GREEN_GENERATION GREEN_FORECAST BROWN_ENERGY_BUY

% ============ ESCENARIO: 11:45 a 14:15 =================

% Max. generaci�n 1.0kW
% 08:00-10:00 0.1kW
% 10:00-11:00 0.2kW
% 11:00-12:00 0.8kW
% 12:00-14:00 1.0kW
% 14:00-15:00 0.8kW
% 15:00-16:00 0.2kW
% 16:00-17:00 0.1kW

GREEN_GENERATION = vertcat(linspace(720, 900, 25)', ...
    ones(100,1) * 900, ...
    linspace(900, 720, 25)');

GREEN_GENERATION = vertcat(rand(25,1) .* 80 + GREEN_GENERATION(1:25), ...
    rand(100,1) .* 100 + GREEN_GENERATION(26:125), ...
    rand(25,1) .* 80 + GREEN_GENERATION(126:150));

% Se considera un 14% de error en la predicci�n
GREEN_FORECAST_ERROR = vertcat(ones(150,1) * ((rand(1,1).*0.14)-(ones(1,1).*0.07)));
GREEN_FORECAST = vertcat(ones(25,1) * mean(GREEN_GENERATION(1:25)), ...
    ones(100,1) * mean(GREEN_GENERATION(26:125)), ...
    ones(25,1) * mean(GREEN_GENERATION(126:150)));
GREEN_FORECAST = GREEN_FORECAST + GREEN_FORECAST .* GREEN_FORECAST_ERROR;

GREEN_GENERATION = GREEN_GENERATION ./ 2;
GREEN_FORECAST = GREEN_FORECAST ./ 2;

%BROWN_ENERGY_BUY = ones(150,1) * 2.885;
BROWN_ENERGY_BUY = vertcat(ones(25,1) * 1.380, ones(100,1) * 2.885, ones(25,1) * 5.600);
% Precios MC2: valle=1.380 llano=2.885 punta=5.600
% Punta: de 18:00 a 22:00 hrs.
% Llano: de 07:00 a 18:00 y de 22:00 a 24:00 hrs.
% Valle: de 00:00 a 07:00 hrs.

GREEN_GENERATION_G2 = GREEN_GENERATION

save g2.mat GREEN_GENERATION GREEN_FORECAST BROWN_ENERGY_BUY

% ============ ESCENARIO: 20:30 a 23:00 =================

% Max. generaci�n 1.0kW
% 08:00-10:00 0.1kW
% 10:00-11:00 0.2kW
% 11:00-12:00 0.8kW
% 12:00-14:00 1.0kW
% 14:00-15:00 0.8kW
% 15:00-16:00 0.2kW
% 16:00-17:00 0.1kW
GREEN_GENERATION = zeros(150,1);
GREEN_FORECAST = zeros(150,1);

BROWN_ENERGY_BUY = vertcat(ones(90,1) * 5.600, ones(60,1) * 2.885);
% Precios MC2: valle=1.380 llano=2.885 punta=5.600
% Punta: de 18:00 a 22:00 hrs.
% Llano: de 07:00 a 18:00 y de 22:00 a 24:00 hrs.
% Valle: de 00:00 a 07:00 hrs.

GREEN_GENERATION_G3 = GREEN_GENERATION

save g3.mat GREEN_GENERATION GREEN_FORECAST BROWN_ENERGY_BUY