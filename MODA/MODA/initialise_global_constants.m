function f = initialise_global_constants(file_workload,file_power_ref,green_power)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    f = [];
    global MAX_SERVERS;        MAX_SERVERS = 1920;
    global MAX_HVAC;           MAX_HVAC = 300;
    global SIMULATION_LENGTH;  SIMULATION_LENGTH = 150;
    global NUMBER_DECISION;    NUMBER_DECISION = SIMULATION_LENGTH*2;
    
    global MAX_NRO_SERVERS;    MAX_NRO_SERVERS = 64;
    
    % Energy consumption data
    global MAX_ENERGY_SERVER; MAX_ENERGY_SERVER = 30;
    global IDLE_ENERGY_SERVER; IDLE_ENERGY_SERVER = 22;
    global SLEEP_ENERGY_SERVER; SLEEP_ENERGY_SERVER = 3;
    
    global WORKLOAD;
    %WORKLOAD = load('W2.txt','-ASCII')
    %WORKLOAD = load('wlarge_1.txt','-ASCII')
    WORKLOAD = load(file_workload,'-ASCII');
    %WORKLOAD(:,2) = round(WORKLOAD(:,2)/2)
    
    global TEMP_REF; global POWER_REF;
    [TEMP_REF, POWER_REF] = get_references(SIMULATION_LENGTH,file_power_ref);
       
    global GREEN_GENERATION;
    global GREEN_FORECAST;
    global BROWN_ENERGY_BUY;
    
    %load('scenarios/powerProfiles/g1.mat');
    %load('scenarios/powerProfiles/g2.mat');
    %load('scenarios/powerProfiles/g3.mat');
    load(green_power);
    
    global GREEN_ENERGY_SELL;
    GREEN_ENERGY_SELL = BROWN_ENERGY_BUY;
    
    global TOTAL_POWER_REF;
    TOTAL_POWER_REF = POWER_REF + GREEN_GENERATION;
       
    global TREND; 
            Tr = load('Tr.mat'); 
            TREND = Tr.Tr;
            
    global ARX_MODEL;
            arx_model = load('arx_model.mat');
            ARX_MODEL = arx_model.arx_model;
end

