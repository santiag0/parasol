%function f = evaluate_objective(x, M, V)
function f = evaluate_objective(x)

%% function f = evaluate_objective(x, M, V)
% Function to evaluate the objective functions for the given input vector
% x. x is an array of decision variables and f(1), f(2), etc are the
% objective functions. The algorithm always minimizes the objective
% function hence if you would like to maximize the function then multiply
% the function by negative one. M is the numebr of objective functions and
% V is the number of decision variables. 
%
% This functions is basically written by the user who defines his/her own
% objective function. Make sure that the M and V matches your initial user
% input. Make sure that the 
%
% An example objective function is given below. It has two six decision
% variables are two objective functions.

% f = [];
% %% Objective function one
% % Decision variables are used to form the objective function.
% f(1) = 1 - exp(-4*x(1))*(sin(6*pi*x(1)))^6;
% sum = 0;
% for i = 2 : 6
%     sum = sum + x(i)/4;
% end
% %% Intermediate function
% g_x = 1 + 9*(sum)^(0.25);
% 
% %% Objective function two
% f(2) = g_x*(1 - ((f(1))/(g_x))^2);

%% Kursawe proposed by Frank Kursawe.
% Take a look at the following reference
% A variant of evolution strategies for vector optimization.
% In H. P. Schwefel and R. M�nner, editors, Parallel Problem Solving from
% Nature. 1st Workshop, PPSN I, volume 496 of Lecture Notes in Computer 
% Science, pages 193-197, Berlin, Germany, oct 1991. Springer-Verlag. 
%
% Number of objective is two, while it can have arbirtarly many decision
% variables within the range -5 and 5. Common number of variables is 3.

global TEMP_REF;
global POWER_REF;
global TREND;
global ARX_MODEL;
global WORKLOAD;
global GREEN_FORECAST;
global GREEN_GENERATION;
global TOTAL_POWER_REF;
global BROWN_ENERGY_BUY;
global GREEN_ENERGY_SELL;
global MAX_ENERGY_SERVER;
global MAX_NRO_SERVERS;
global dim;

f = [];

sim_period = dim/2;

%smoothing meanSP
windowFan = 21;
windowSP = 400;
windowServers = 90;
windowT14 = 210;

%display(sprintf('(1) Elapsed: %.2f', cputime-timeStart));

Setpoint = TEMP_REF';%repmat(30,1,sim_period);
%Fan = repmat(0,1,sim_period);
%Servers = repmat(1800,1,sim_period);
T14 = repmat(25,1,sim_period);

%0 <=x(i) <= 100 means that the NV is on and the fan runs at x(i) speed (%)
%100 <x(i) <= 200 means that the AC is on and the Fan is off
%200 <x(i) <= 300 means that both AC and Fan are off

cooling = round(x(1:round(sim_period)));
Servers = round(x(round(sim_period)+1:dim)); %the last two elements are the objective values

AC = (cooling>100 & cooling<200);
Fan = (cooling.*(cooling<=100));

Damper = Fan > 0;

%display(sprintf('(2) Elapsed: %.2f', cputime-timeStart));

meanSP = Setpoint; 
meanFan = Fan; 
meanServers = Servers; 
meanT14 = T14; 
meanAC = AC; 
fromServers = 1; fromFan = 1; fromSP = 1; fromT14 = 1; to = 2;
for i=2:sim_period
%    meanSP2(i) = mean(Setpoint(fromSP:to));
% A MUCH MOR EFFICIENT WAY TO COMPUTE THE MOVING AVERAGE:
     leavingSP = 0;
     correctionSP = 1;
     if fromSP > 1
         leavingSP = Setpoint(fromSP-1)/(to-fromSP+1);
     else
         correctionSP = (to-fromSP) / (to-fromSP+1);
     end
    meanSP(i) = correctionSP*meanSP(i-1) + Setpoint(i)/(to-fromSP+1) - leavingSP;

%    meanT14(i) = mean(T14(fromT14:to));
% A MUCH MOR EFFICIENT WAY TO COMPUTE THE MOVING AVERAGE:
    leavingT14 = 0;
     correctionT14 = 1;
     if fromT14 > 1
         leavingT14 = T14(fromT14-1)/(to-fromT14+1);
     else
         correctionT14 = (to-fromT14) / (to-fromT14+1);
     end
    meanT14(i) = correctionT14*meanT14(i-1) + T14(i)/(to-fromT14+1) - leavingT14;

    
    %display(sprintf('Entering: %.2f, Leaving: %.2f', Setpoint(i)/(to-fromSP+1), leavingSP));    
    meanFan(i) = mean(Fan(fromFan:to));
    meanServers(i) = mean(Servers(fromServers:to));

    to = to + 1;
    fromSP = max(1,to-windowSP+1);
    fromServers = max(1,to-windowServers+1);
    fromFan = max(1,to-windowFan+1);
    fromT14 = max(1,to-windowT14+1);
end

%figure(); plot(meanT14); hold on; plot(meanT142, 'r'); hold off;


%display(sprintf('MeanSP: %.2f, MeanT14:%.2f\n', meanSP, meanT14));

%figure; plot(meanT14); hold on; plot(meanSP, 'g'); hold off;

U2 = Damper.*(((1-AC).*meanFan).^(1/1)).*abs(meanSP-meanT14).^(1/9).*sign(meanSP-meanT14);

U2 = max(U2,0) - 18.6571;

%display(size([meanAC; U2;  T14; meanServers; Damper; meanSP]));

U = [meanAC; U2;  T14; meanServers; Damper; meanSP]';
for i = 1:size(U,2)
    U(:,i) = U(:,i)-TREND.InputOffset(i);
end

%display(sprintf('(5) Elapsed: %.2f', cputime-timeStart));
%U = U - repmat(Tr.InputOffset,size(U,1),1);

%PARA DEFINIR LA TEMPERATURA INTERNA INICIAL, CAMBIAR starting_temp
starting_temp = 25;
first_temp_change = 0.1;
det_temp = starting_temp - TREND.OutputOffset;
initial_conditions = [det_temp; det_temp + ARX_MODEL.A(1,2)*(det_temp-first_temp_change)-(U(1,:)*ARX_MODEL.B(:,2))];

y = sim(ARX_MODEL,U, initial_conditions);

coolingPower = AC*2300 + (Fan/100)*410;
% OLD: power not adjusted by the scheduling
% power = coolingPower+Servers;

%ServersGreen = rand(1,150) * 1000;
%ServersBrown = Servers;
%Servers = ServersBrown + ServersGreen;

diff_temp = (y + TREND.OutputOffset) - (ones(150,1) .* 27); %TEMP_REF;
%diff_temp = 0;

if max(diff_temp) > 0
   %f(1) = sum((ones(1,150) * MAX_NRO_SERVERS * MAX_ENERGY_SERVER) / max(TOTAL_POWER_REF));
   f(1) = 100000 * sim_period * MAX_NRO_SERVERS * MAX_ENERGY_SERVER;
   f(2) = 100000 * (ones(sim_period,1) * MAX_NRO_SERVERS * MAX_ENERGY_SERVER)' * BROWN_ENERGY_BUY;
   f(3) = 100000 * sum(WORKLOAD(:,2));
else
    [assign,mak,n_violations,t_violations,server_power,off_sim] = scheduler(WORKLOAD,Servers,dim,coolingPower);
    
    if (mak <= 150)     
        power = coolingPower+server_power;
        diff_power = power'-TOTAL_POWER_REF;
        diff_power(diff_power < 0) = 0;
    
        %brown_power = power - GREEN_FORECAST';
        brown_power = power - GREEN_GENERATION';
        power_selled = brown_power;
        power_selled(power_selled > 0) = 0;
    
        power_bought = brown_power;
        power_bought(brown_power < 0) = 0;
    
        %fitness_budget = (power_bought * BROWN_ENERGY_BUY) + (power_selled * GREEN_ENERGY_SELL);
        fitness_budget = (power_bought * BROWN_ENERGY_BUY); % + (power_selled * GREEN_ENERGY_SELL);
        %fitness_budget = fitness_budget / sum((150 * MAX_NRO_SERVERS * MAX_ENERGY_SERVER) * BROWN_ENERGY_BUY);
    
        fitness_pwr = sum(diff_power); %sum(diff_power/max(TOTAL_POWER_REF));

        f(1) = fitness_pwr;
        f(2) = fitness_budget;
        f(3) = t_violations;
   else
       %%f(1) = sum((ones(1,150) * MAX_NRO_SERVERS * MAX_ENERGY_SERVER) / max(TOTAL_POWER_REF));
       f(1) = 100000 * sim_period * MAX_NRO_SERVERS * MAX_ENERGY_SERVER;
       f(2) = 100000 * (ones(sim_period,1) * MAX_NRO_SERVERS * MAX_ENERGY_SERVER)' * BROWN_ENERGY_BUY;
       f(3) = 100000 * sum(WORKLOAD(:,2));
   end
end


%USE THIS WHEN PLOTTING THE SOLUTION AFTER RUNNING THE GA
%global DATA_FOR_PLOTTING;
%DATA_FOR_PLOTTING = struct('y',y, 'u',U, 'power', coolingPower + Servers, 'coolingPower', coolingPower, 'serversPower', Servers, 'fitness', 0);
%DATA_FOR_PLOTTING = struct('y',y, 'u',U, 'power', coolingPower + Servers, 'coolingPower', coolingPower, 'serversPower', Servers, 'fitness', 0, 'nv', n_violations, 'tv', t_violations);

% DEBUG
% disp(sprintf('(%d,%.2f,%.2f)',f(1),f(2),f(3)));

%% Check for error
%if length(f) ~= M
%    error('The number of decision variables does not match you previous input. Kindly check your objective function');
%end